package sandbox.dependency.playground

import java.nio.charset.{Charset, StandardCharsets}

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class JwtBase64Spec extends AnyFlatSpec with Matchers {

  val token = "eyJraWQiOiI4anNsTG5HbmZFaG4wXzJJYmE0bjRmRVprbW0zMkpFSUExMWF0U2tfSkdvIiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI2MjFkZTNhOGQ4YzVlNzAzOGM1YzE1YTFmMzA1YWE3ZjQ0Y2JhNTBjIiwiYXVkIjoiY2FycmVmb3VyX29uZWNhcnJlZm91cl93ZWIiLCJDTEkiOiIxM2M1MzE5ZS0yODRlLTQ4ZGEtOTljNS0zYTEzMGEyYWFmOGQiLCJDU1MiOnsiRklEIjpbIjkxMzU3MjAwMDA2ODM1Nzk0MDciXSwiVUZOIjoiRHVwb25UIEJvYiIsIlVFTSI6InR0ZGRzZEBqb3VycmFwaWRlLmNvbSJ9LCJpc3MiOiJTZWN1cmVfVG9rZW5fU2VydmVyIiwiVE9LRU5fVFlQRSI6IkpXVFRva2VuIiwiR1JTIjpbIndzX1N0b3JlRk9fUiIsIndzX1JlZmVyZW50aWFsX1IiLCJTdG9yZUZPX1IiLCJ3c19DdXN0b21lcl9SUiIsInNhbGVfdHJhbnNhY3Rpb25fa2V5X1IiLCJjdXN0b21lcl9zYWxlc19sb3lhbHR5X1IiXSwiZXhwIjoxNTU1NjgyNjQ3LCJpYXQiOjE1NTU2NjgyNDd9.pxyE7SFDtV7miQEGHJ8TX0p2MrI1FwM6nF4mrKSLgH28LFiiwjblIi5cIh2W00bWSVt_FhvxtUbz1t1NRVIokaVKhy3OyyT4nqNDoxNKQ90vnUglsYlLosAjmW_pOCQYpC-qKPwoQqNx_iSIZhFBeteEw_u65DmsMe6s4vz_JbEQ9JMH32XwGBLBtnkvyfJVRZPl0-eDlNhIlFREIy9Mij5xhcfvDLLGwxql5s7nYIreXfb8IrmsQZJHbarkGQFTXhF68EsWZkHDearuhqMY86qpd_qQq7O0CeWVVIjpwuLFUqf_9MHDArvP2QkYRYo3weYlzwb6xAjibprviNPJCw"

  "toto" should "do something" in {
    println(Base64UrlEncoding.base64UrlDecodeAsString(token.split('.')(0)))
  }

}

/**
  * Base64Url encoding and decoding following rfc7515#section-2, 'Base64url Encoding'
  * (which redirect to rfc4648#section-5) and the implementation note
  * rfc7515#appendix-C
  * https://tools.ietf.org/html/rfc7515#section-2
  * https://tools.ietf.org/html/rfc4648#section-5
  * https://tools.ietf.org/html/rfc7515#appendix-C
  */
object Base64UrlEncoding {
  def base64UrlEncode(decodedStr: Array[Byte]): String = {
    val Base64url = true
    val decoded: Array[Byte] = org.apache.commons.codec.binary.Base64.encodeBase64(decodedStr, false, Base64url)
    val encodedStr: String = new String(decoded)

    encodedStr.split('=')(0) // Remove any trailing '='s
      .replace('+', '-') // 62nd char of encoding
      .replace('/', '_') // 63rd char of encoding
  }

  def base64UrlDecodeAsByteArray(encodedStr: String): Array[Byte] = {
    val cleanedEncodedStr = encodedStr.split('=')(0) // Remove any trailing '='s
      .replace('-', '+') // 62nd char of encoding
      .replace('_', '/') // 63rd char of encoding

    val finalEncodedStr = cleanedEncodedStr.length % 4 match {
      case 0 => cleanedEncodedStr
      case 2 => cleanedEncodedStr + "=="
      case 3 => cleanedEncodedStr + "="
      case _ => throw new RuntimeException("Bad base64url string")
    }
    org.apache.commons.codec.binary.Base64.decodeBase64(finalEncodedStr)
  }
  def base64UrlDecodeAsString(encodedStr: String, charset: Charset = StandardCharsets.UTF_8): String = {
    new String(base64UrlDecodeAsByteArray(encodedStr), charset)
  }
}