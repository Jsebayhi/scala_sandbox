package sandbox.dependency.playground.json4s

import org.json4s.DefaultFormats
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import scala.reflect.ClassTag
import scala.util.Try

class DeserializationSpec extends AnyFlatSpec with Matchers {

  it should "not deserialize an empty json object" in deserialize[MandatoryContent]("{}") { res =>
    res.isSuccess shouldBe false
  }

  it should "deserialize a json object without optional value" in deserialize[OptionalContent]("""{}""") { res =>
    res.isSuccess shouldBe true
    res shouldBe Try(OptionalContent())
  }

  it should "deserialize a json object with optional value" in deserialize[OptionalContent]("""{"opt":"filled"}""") { res =>
    res.isSuccess shouldBe true
    res shouldBe Try(OptionalContent(Some("filled")))
  }

  def deserialize[T: ClassTag](json: String)(f: Try[T] => Unit)(implicit m: Manifest[T]) = {
    import org.json4s.jackson.JsonMethods.parse
    implicit val defaultFormat = DefaultFormats
    f(Try(parse(json).extract[T]))
  }
}

case class OptionalContent(opt: Option[String] = None)
case class MandatoryContent(str: String)

