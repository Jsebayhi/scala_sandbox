package sandbox.dependency.playground.avro.interpreter.ast6.behavior

import org.apache.avro.generic.GenericRecord

trait Containsable[T] {
  def doesContain(container: GenericRecord, candidate: T): Boolean
}
