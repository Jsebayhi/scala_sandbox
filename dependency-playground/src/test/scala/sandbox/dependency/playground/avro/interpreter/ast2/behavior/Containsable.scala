package sandbox.dependency.playground.avro.interpreter.ast2.behavior

trait Containsable {
  def isContained(fieldContent: AnyRef): Boolean
}