package sandbox.scalapl.interpreter.playground.model

import org.apache.avro.Schema
import org.apache.avro.generic.GenericData.Record

case class StringId(id: String) {

  val schema =
    s"""
       |{
       |  "name": "${this.getClass.getSimpleName}",
       |  "type": "record",
       |  "fields": [
       |    {
       |      "name": "id",
       |      "type": "string"
       |    }
       |  ]
       |}
    """.stripMargin

  val avroSchema = new Schema.Parser().parse(schema)

  def toGenericAvro: Record = {
    val r = new Record(avroSchema)
    r.put("id", id)
    r
  }
}

case class IntId(id: Int) {

  val schema =
    s"""
       |{
       |  "name": "${this.getClass.getSimpleName}",
       |  "type": "record",
       |  "fields": [
       |    {
       |      "name": "id",
       |      "type": "int"
       |    }
       |  ]
       |}
    """.stripMargin

  val avroSchema = new Schema.Parser().parse(schema)

  def toGenericAvro: Record = {
    val r = new Record(avroSchema)
    r.put("id", id)
    r
  }
}
