package sandbox.dependency.playground.avro.interpreter.ast5

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.behavior.Equalsable

case class Equals[T](fieldAccessor: Equalsable[T], v: T) extends Expression {
  override def eval(container: GenericContainer[AnyRef]): Boolean = {
    fieldAccessor.isEqual(container, v)
  }
}