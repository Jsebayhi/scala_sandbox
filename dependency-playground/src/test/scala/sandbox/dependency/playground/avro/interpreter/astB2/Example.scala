
package sandbox.dependency.playground.avro.interpreter.astB2

import com.carrefour.phenix.tradeitem.mapping.playground.model._
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class Example extends AnyFlatSpec with Matchers {

  val tony = Person("Tony").toGenericAvro
  val nestedTony = Nested(Person("Tony")).toGenericAvro
  val shoppingList = ShoppingList(items = List("apple", "хлеб", "картофель")).toGenericAvro
  val id1 = IntId(1).toGenericAvro

  val findTony = RuleFilterAST(
    PredExpr(
      Contains(StringPromise(Seq("name")), StringPrim("Tony"))
    )
  )

  val findNestedTony = RuleFilterAST(
    PredExpr(
      Contains(StringPromise(Seq("person", "name")), StringPrim("Tony"))
    )
  )

  val findNotTony = RuleFilterAST(
    NotExpr(PredExpr(
      Contains(StringPromise(Seq("name")), StringPrim("Tony"))
    ))
  )

  val findAppleInTheShoppingList = RuleFilterAST(
    PredExpr(
      Contains(ArrayStringPromise(Seq("items")), StringPrim("apple"))
    )
  )

  val findId1 = RuleFilterAST(
    PredExpr(
      Equals(IntPromise(Seq("id")), IntPrim(1))
    )
  )

  it should "find nested tony" in {
    val res = InterpreterV0.eval(findNestedTony)(nestedTony)
    assert(res.isSuccess)
    assert(res.get)
  }

  it should "find tony" in {
    val res = InterpreterV0.eval(findTony)(tony)
    assert(res.isSuccess)
    assert(res.get)
  }

  it should "find the apple in the shopping list" in {
    val res = InterpreterV0.eval(findAppleInTheShoppingList)(shoppingList)
    assert(res.isSuccess)
    assert(res.get)
  }

  it should "find the correct id" in {
    val res = InterpreterV0.eval(findId1)(id1)
    assert(res.isSuccess)
    assert(res.get)
  }

}
