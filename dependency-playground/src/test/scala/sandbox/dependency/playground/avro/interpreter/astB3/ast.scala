
package sandbox.scalapl.interpreter.playground.astB3

/**
 * The AST of a mapping rule filter.
 *
 * For example, the following filter:
 *
 * {{{
 * name.contains("Apple") and not id.equals("10")
 * }}}
 *
 * will be represented by the following data structure:
 *
 * {{{
 * RuleFilterAST(
 *   AndExpr(
 *     PredExpr(
 *       Operation(field = "name", op = Contains, prim = StringPrim("Apple"))
 *     ),
 *     NotExpr(
 *       PredExpr(
 *         Equals(field = "id", prim = IntPrim(10))
 *       )
 *     )
 *   )
 * )
 * }}}
 */
case class RuleFilterAST(expr: LogicalExpr)

sealed trait LogicalExpr
case class OrExpr(left: LogicalExpr, right: LogicalExpr) extends LogicalExpr
case class AndExpr(left: LogicalExpr, right: LogicalExpr) extends LogicalExpr
case class NotExpr(expr: LogicalExpr) extends LogicalExpr
case class PredExpr(pred: Predicate) extends LogicalExpr

sealed trait Predicate
case class ContainsString(promise: RecordPromise with StringPrimContainsable, argument: StringPrim) extends Predicate
case class EqualsString(promise: RecordPromise with StringPrimEqualsable, argument: StringPrim) extends Predicate
case class StartsWithString(promise: RecordPromise with StringPrimStartsWithable, argument: StringPrim) extends Predicate
case class EqualsInt(promise: RecordPromise with IntPrimEqualsable, argument: IntPrim) extends Predicate

// Primitive types are only compatible with strings for beginning since strings will cover 90% cases.
// Other types (int, booleans, arrays... Can be added afterward)
sealed trait Primitive
case class StringPrim(lit: String) extends Primitive with StringPrimBehavior
case class IntPrim(lit: Int) extends Primitive with IntPrimBehavior
case class ArrayStringPrim(l: Seq[String]) extends Primitive with ArrayStringPrimBehavior

/**
 * A promise regarding a record's primitive content.
 */
sealed trait RecordPromise
case class StringPromise(fieldSequence: Seq[String]) extends RecordPromise with StringPrimBehavior
case class IntPromise(fieldSequence: Seq[String]) extends RecordPromise with IntPrimBehavior
case class ArrayStringPromise(fieldSequence: Seq[String]) extends RecordPromise with ArrayStringPrimBehavior

// Primitive's Behaviors description

trait StringPrimBehavior
  extends StringPrimContainsable
  with StringPrimEqualsable
  with StringPrimStartsWithable
  with IntPrimEqualsable

trait IntPrimBehavior
  extends IntPrimEqualsable
  with StringPrimEqualsable

trait ArrayStringPrimBehavior
  extends StringPrimContainsable

/**
 * A primitive possible behavior
 */
sealed trait Behavior
trait StringPrimContainsable extends Behavior
trait StringPrimEqualsable extends Behavior
trait StringPrimStartsWithable extends Behavior
trait IntPrimEqualsable extends Behavior
