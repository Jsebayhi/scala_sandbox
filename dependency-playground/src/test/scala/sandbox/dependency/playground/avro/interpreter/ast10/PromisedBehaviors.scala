package sandbox.scalapl.interpreter.playground.ast10

import org.apache.avro.generic.GenericRecord

trait Containsable[T] {
  def doesContain(container: GenericRecord, candidate: T): Boolean
}

trait Equalsable[T] {
  def isEqual(container: GenericRecord, fieldContent: T): Boolean
}

trait StartsWithable[T] {
  def doesStartsWith(container: GenericRecord, fieldContent: T): Boolean
}