package sandbox.dependency.playground.avro.interpreter.ast2

import scala.util.{ Failure, Try }

import com.carrefour.phenix.tradeitem.mapping.playground.ast2.behavior._
case class StringPrim(v: String) extends Containsable
  with Equalsable
  with StartsWithable {

  private def cast(a: AnyRef): String = Try(a.asInstanceOf[String])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a String", e)) }
    .get

  override def isContained(fieldContent: AnyRef): Boolean = {
    cast(fieldContent).contains(v)
  }

  override def isEqual(fieldContent: AnyRef): Boolean = {
    cast(fieldContent) == v
  }

  override def doesStartsWith(fieldContent: AnyRef): Boolean = {
    cast(fieldContent).startsWith(v)
  }
}