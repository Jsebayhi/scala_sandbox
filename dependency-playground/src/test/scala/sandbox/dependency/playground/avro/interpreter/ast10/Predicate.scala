package sandbox.dependency.playground.avro.interpreter.ast10

import org.apache.avro.generic.GenericRecord

trait Predicate extends Expression

case class Contains[T](fieldAccessor: Containsable[T], v: T) extends Predicate {
  override def eval(container: GenericRecord): Boolean = {
    fieldAccessor.doesContain(container, v)
  }
}

case class Equals[T](fieldAccessor: Equalsable[T], v: T) extends Predicate {
  override def eval(container: GenericRecord): Boolean = {
    fieldAccessor.isEqual(container, v)
  }
}

case class StartsWith[T](fieldAccessor: StartsWithable[T], v: T) extends Predicate {
  override def eval(container: GenericRecord): Boolean = {
    fieldAccessor.doesStartsWith(container, v)
  }
}