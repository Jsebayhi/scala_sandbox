package sandbox.dependency.playground.avro.interpreter.model

import com.carrefour.phenix.tradeitem.mapping.playground.{ AnyRefContainer, GenericContainer }
import org.apache.avro.Schema
import org.apache.avro.generic.GenericData.Record

case class Person(name: String) {

  def toGenericContainer: GenericContainer[AnyRef] = {
    AnyRefContainer().set("name", name)
  }

  val schema =
    s"""
      |{
      |  "name": "${this.getClass.getSimpleName}",
      |  "type": "record",
      |  "fields": [
      |    {
      |      "name": "name",
      |      "type": "string"
      |    }
      |  ]
      |}
    """.stripMargin

  val avroSchema = new Schema.Parser().parse(schema)

  def toGenericAvro: Record = {
    val r = new Record(avroSchema)
    r.put("name", name)
    r
  }
}