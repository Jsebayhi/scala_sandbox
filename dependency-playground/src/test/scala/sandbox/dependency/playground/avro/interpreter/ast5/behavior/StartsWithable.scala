package sandbox.dependency.playground.avro.interpreter.ast5.behavior

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer

trait StartsWithable[T] {
  def doesStartsWith(container: GenericContainer[AnyRef], fieldContent: T): Boolean
}