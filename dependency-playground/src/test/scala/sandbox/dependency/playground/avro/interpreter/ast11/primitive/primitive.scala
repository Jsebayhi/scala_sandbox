package sandbox.dependency.playground.avro.interpreter.ast11.primitive

import com.carrefour.phenix.tradeitem.mapping.playground.ast11.Primitive

// Fix: a case class cannot inherit Containsable[String] and Containsable[Int]
trait Containsable[T] {
  def doesContain(candidate: Primitive[T]): Boolean
}

trait Equalsable[T] {
  def isEqual(fieldContent: Primitive[T]): Boolean
}

trait StartsWithable[T] {
  def doesStartsWith(fieldContent: Primitive[T]): Boolean
}