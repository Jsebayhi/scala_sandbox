
package sandbox.scalapl.interpreter.playground.astB1

/**
 * The AST of a mapping rule filter.
 *
 * For example, the following filter:
 *
 * {{{
 * name.contains("Apple") and not id.equals("10")
 * }}}
 *
 * will be represented by the following data structure:
 *
 * {{{
 * RuleFilterAST(
 *   AndExpr(
 *     PredExpr(
 *       Operation(field = "name", op = Contains, prim = StringPrim("Apple"))
 *     ),
 *     NotExpr(
 *       PredExpr(
 *         Equals(field = "id", prim = IntPrim(10))
 *       )
 *     )
 *   )
 * )
 * }}}
 */
case class RuleFilterAST(expr: LogicalExpr)

sealed trait LogicalExpr
case class OrExpr(left: LogicalExpr, right: LogicalExpr) extends LogicalExpr
case class AndExpr(left: LogicalExpr, right: LogicalExpr) extends LogicalExpr
case class NotExpr(expr: LogicalExpr) extends LogicalExpr
case class PredExpr(pred: Predicate) extends LogicalExpr

sealed trait Predicate
case class Contains(fieldSequence: Seq[String], argument: Primitive) extends Predicate
case class Equals(fieldSequence: Seq[String], argument: Primitive) extends Predicate
case class StartsWith(fieldSequence: Seq[String], argument: Primitive) extends Predicate

// Primitive types are only compatible with strings for beginning since strings will cover 90% cases.
// Other types (int, booleans, nested records, arrays... Can be added afterward)
sealed trait Primitive
case class StringPrim(lit: String) extends Primitive
case class ArrayStringPrim(l: Seq[String]) extends Primitive
