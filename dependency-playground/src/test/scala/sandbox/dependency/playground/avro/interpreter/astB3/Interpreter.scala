
package sandbox.dependency.playground.avro.interpreter.astB3

import com.carrefour.phenix.tradeitem.mapping.playground.astB3.GenericRecordAccessor._
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord

import scala.reflect.ClassTag
import scala.util.{ Failure, Success, Try }

trait Interpreter {
  def eval(ast: RuleFilterAST)(record: GenericRecord): Try[Boolean]
}

//TODO: add error context with recoverWith{case e => Failure(MoreContext(e))}
object InterpreterV0 extends Interpreter {

  override def eval(ast: RuleFilterAST)(record: GenericRecord): Try[Boolean] = evalLogicalExpr(ast.expr)(record)

  def evalLogicalExpr(expr: LogicalExpr)(record: GenericRecord): Try[Boolean] = expr match {
    case OrExpr(l, r)  => evalLogicalExpr(l)(record).flatMap(lr => evalLogicalExpr(r)(record).map(_ || lr))
    case AndExpr(l, r) => evalLogicalExpr(l)(record).flatMap(lr => evalLogicalExpr(r)(record).map(_ && lr))
    case NotExpr(e)    => evalLogicalExpr(e)(record).map(res => !res)
    case PredExpr(p)   => evalPredicate(p)(record)
  }

  def evalPredicate(predicate: Predicate)(record: GenericRecord): Try[Boolean] = predicate match {
    case ContainsString(promise, argument)   => evalPromise[StringPrimContainsable](promise)(record).flatMap(evalContainsString(_, argument))
    case EqualsString(promise, argument)     => evalPromise[StringPrimEqualsable](promise)(record).flatMap(evalEqualsString(_, argument))
    case StartsWithString(promise, argument) => evalPromise[StringPrimStartsWithable](promise)(record).flatMap(evalStartWithString(_, argument))
    case EqualsInt(promise, argument)        => evalPromise[IntPrimEqualsable](promise)(record).flatMap(evalEqualsInt(_, argument))
  }

  def evalPromise[T <: Behavior: ClassTag](promise: RecordPromise)(record: GenericRecord): Try[Primitive with T] = {
    extractPromise(promise)(record)
      .flatMap(assertPrimitiveBehavior[T])
  }

  def extractPromise(promise: RecordPromise)(record: GenericRecord): Try[Primitive] = {
    promise match {
      case StringPromise(path)      => extractString(path)(record)
      case IntPromise(path)         => extractInt(path)(record)
      case ArrayStringPromise(path) => extractArrayString(path)(record)
    }
  }

  def extractString(path: Seq[String])(record: GenericRecord): Try[StringPrim] = {
    extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.STRING =>
        Success(StringPrim(obj.asInstanceOf[String].trim))
    }
  }

  def extractInt(path: Seq[String])(record: GenericRecord): Try[IntPrim] = {
    extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.INT =>
        Success(IntPrim(obj.asInstanceOf[Int]))
    }
  }

  def extractArrayString(path: Seq[String])(record: GenericRecord): Try[ArrayStringPrim] = {
    extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.ARRAY =>
        Success(ArrayStringPrim(obj.asInstanceOf[List[String]].map(_.trim)))
    }
  }

  def assertPrimitiveBehavior[T <: Behavior: ClassTag](prim: Primitive): Try[Primitive with T] = {
    prim match {
      case p: T => Success(p)
      case p    => Failure(new RuntimeException(s"$p do not have expected behavior"))
    }
  }

  def evalContainsString(
    fieldContent: Primitive with StringPrimContainsable,
    argument: StringPrim): Try[Boolean] = fieldContent match {
    case StringPrim(s)      => Success(s.contains(argument.lit))
    case ArrayStringPrim(l) => Success(l.contains(argument.lit))
  }

  def evalEqualsString(
    fieldContent: Primitive with StringPrimEqualsable,
    argument: StringPrim): Try[Boolean] = fieldContent match {
    case StringPrim(str) => Success(str == argument.lit)
    case IntPrim(i)      => Success(argument.lit == i.toString)
  }

  def evalStartWithString(
    fieldContent: Primitive with StringPrimStartsWithable,
    argument: StringPrim): Try[Boolean] = fieldContent match {
    case StringPrim(s1) => Success(s1.startsWith(argument.lit))
  }

  def evalEqualsInt(
    fieldContent: Primitive with IntPrimEqualsable,
    argument: IntPrim): Try[Boolean] = fieldContent match {
    case StringPrim(str) => Success(str == argument.lit.toString)
    case IntPrim(i)      => Success(argument.lit == i)
  }
}
