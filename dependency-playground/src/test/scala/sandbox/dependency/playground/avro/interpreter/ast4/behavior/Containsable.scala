package sandbox.dependency.playground.avro.interpreter.ast4.behavior

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer

trait Containsable[T] {
  def doesContain(container: GenericContainer[AnyRef], candidate: T): Boolean
}
