package sandbox.dependency.playground.avro.interpreter.ast12.example

import com.carrefour.phenix.tradeitem.mapping.playground.ast12._
import com.carrefour.phenix.tradeitem.mapping.playground.model
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Success

class ast12Example extends AnyFlatSpec with Matchers {

  it should "identify approximately timy" in {
    val data = model.Person("I am not Timy!").toGenericAvro

    ContainsString(PromisedStringPrim(Seq("name")), StringPrim("Timy"))
      .eval(data) shouldBe Success(true)
  }

  it should "identify timy" in {
    val data = model.Person("Timy").toGenericAvro

    EqualsString(PromisedStringPrim(Seq("name")), StringPrim("Timy"))
      .eval(data) shouldBe Success(true)
  }

  it should "identify not timy" in {
    val data = model.Person("I am not Timy!").toGenericAvro

    Not(EqualsString(PromisedStringPrim(Seq("name")), StringPrim("Timy")))
      .eval(data) shouldBe Success(true)
  }

  it should "identify that timy starts with 'tim'" in {
    val data = model.Person("Timy").toGenericAvro

    StartsWithString(PromisedStringPrim(Seq("name")), StringPrim("Tim"))
      .eval(data) shouldBe Success(true)
  }

  it should "identify an id which is 0" in {
    val data = model.IntId(0).toGenericAvro

    EqualsInt(PromisedIntPrim(Seq("id")), IntPrim(0))
      .eval(data) shouldBe Success(true)
  }
  it should "identify an id which is \"0\"" in {
    val data = model.IntId(0).toGenericAvro

    EqualsString(PromisedIntPrim(Seq("id")), StringPrim("0"))
      .eval(data) shouldBe Success(true)
  }

  it should "identify a string id which is equals to 0" in {
    val data = model.StringId("0").toGenericAvro

    EqualsInt(PromisedStringPrim(Seq("id")), IntPrim(0))
      .eval(data)
  }

  it should "identify an apple" in {
    val data = model.ShoppingList(List(
      "banana",
      "apple"
    )).toGenericAvro

    ContainsString(PromisedListStringPrim(Seq("items")), StringPrim("apple"))
      .eval(data) shouldBe Success(true)
  }
  /*
  it should "identify the number 1" in {
    val data = model.NumberList(List(0, 1)).toGenericAvro

    ContainsString(PromisedListPrim[Int](Seq("numbers")), IntPrim(1))
      .eval(data) shouldBe Success(true)
  }
*/
  it should "handle nested" in {
    val data = model.Nested(model.Person("toto")).toGenericAvro

    ContainsString(PromisedStringPrim(Seq("person", "name")), StringPrim("toto"))
      .eval(data) shouldBe Success(true)

  }
}
