package sandbox.dependency.playground.avro.interpreter

import scala.collection.mutable

trait GenericContainer[T] {
  def set(k: String, v: T): GenericContainer[T]
  def get(k: String): Option[T]
  def schema(): String
}

class MapContainer[T] extends GenericContainer[T] {

  val m = mutable.Map.empty[String, T]

  override def get(k: String): Option[T] = m.get(k)

  override def set(k: String, v: T): GenericContainer[T] = {
    m.update(k, v)
    this
  }

  override def schema(): String = m.keys.toString()
}

object AnyRefContainer {
  def apply(): AnyRefContainer = new MapContainer[AnyRef]
  type AnyRefContainer = GenericContainer[AnyRef]
}

