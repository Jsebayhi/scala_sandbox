package sandbox.dependency.playground.avro.interpreter.ast6

import org.apache.avro.generic.GenericRecord

case class Or(left: Expression, right: Expression) extends Expression {

  override def eval(container: GenericRecord): Boolean = {
    left.eval(container) || right.eval(container)
  }

}
