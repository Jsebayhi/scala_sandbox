package sandbox.dependency.playground.avro.interpreter.ast3

import com.carrefour.phenix.tradeitem.mapping.playground.AnyRefContainer.AnyRefContainer

case class Not(expr: Expression) extends Expression {
  override def eval(container: AnyRefContainer): Boolean = {
    !expr.eval(container)
  }
}