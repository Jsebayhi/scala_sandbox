package sandbox.dependency.playground.avro.interpreter.ast5

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.behavior.StartsWithable

case class StartsWith[T](fieldAccessor: StartsWithable[T], v: T) extends Expression {
  override def eval(container: GenericContainer[AnyRef]): Boolean = {
    fieldAccessor.doesStartsWith(container, v)
  }
}