package sandbox.dependency.playground.avro.interpreter.model

import com.carrefour.phenix.tradeitem.mapping.playground.{ AnyRefContainer, GenericContainer }
import org.apache.avro.Schema
import org.apache.avro.generic.GenericData.Record

case class NumberList(numbers: List[Int]) {

  def toGenericContainer: GenericContainer[AnyRef] = {
    AnyRefContainer().set("numbers", numbers)
  }

  val schema =
    s"""
      |{
      |  "name": "${this.getClass.getSimpleName}",
      |  "type": "record",
      |  "fields": [
      |    {
      |      "name": "numbers",
      |      "type": {
      |        "type": "array",
      |        "items": "int"
      |      }
      |    }
      |  ]
      |}
    """.stripMargin

  val avroSchema = new Schema.Parser().parse(schema)

  def toGenericAvro: Record = {
    val r = new Record(avroSchema)
    r.put("numbers", numbers)
    r
  }
}
