package sandbox.dependency.playground.avro.interpreter.ast5.example

import com.carrefour.phenix.tradeitem.mapping.playground.ast5._
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.field._
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.accessor._
import com.carrefour.phenix.tradeitem.mapping.playground.model
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class Ast5 extends AnyFlatSpec with Matchers {

  it should "identify approximately timy" in {
    val data = model.Person("I am not Timy!").toGenericContainer

    Contains(StringField(Direct("name")), "Timy")
      .eval(data) shouldBe true
  }

  it should "identify timy" in {
    val data = model.Person("Timy").toGenericContainer

    Equals(StringField(Direct("name")), "Timy")
      .eval(data) shouldBe true
  }

  it should "identify not timy" in {
    val data = model.Person("I am not Timy!").toGenericContainer

    Not(Equals(StringField(Direct("name")), "Timy"))
      .eval(data) shouldBe true
  }

  it should "identify that timy starts with 'tim'" in {
    val data = model.Person("Timy").toGenericContainer

    StartsWith(StringField(Direct("name")), "Tim")
      .eval(data) shouldBe true
  }

  it should "identify an apple" in {
    val data = model.ShoppingList(List(
      "banana",
      "apple"
    )).toGenericContainer

    Contains(ListField[String](Direct("items")), "apple")
      .eval(data) shouldBe true
  }

  it should "identify the number 1" in {
    val data = model.NumberList(List(0, 1)).toGenericContainer

    Contains(ListField[Int](Direct("numbers")), 1)
      .eval(data) shouldBe true
  }

  it should "handle nested" in {
    val data = model.Nested(model.Person("toto")).toGenericContainer

    Contains(StringField(Nested("person.name")), "toto")
      .eval(data) shouldBe true

  }
}
