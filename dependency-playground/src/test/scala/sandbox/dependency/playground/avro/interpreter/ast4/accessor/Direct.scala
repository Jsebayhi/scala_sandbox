package sandbox.dependency.playground.avro.interpreter.ast4.accessor

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer

case class Direct(fieldName: String) extends Accessor {

  override def access(container: GenericContainer[AnyRef]): AnyRef = {
    container.get(fieldName)
      .getOrElse(throw new RuntimeException(s"incompatible schema, the field $fieldName is unknown to this schema (${container.schema()}"))
  }
}
