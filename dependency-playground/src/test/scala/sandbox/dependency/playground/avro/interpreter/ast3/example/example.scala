package sandbox.dependency.playground.avro.interpreter.ast3.example

import com.carrefour.phenix.tradeitem.mapping.playground.ast3._
import com.carrefour.phenix.tradeitem.mapping.playground.ast3.field._
import com.carrefour.phenix.tradeitem.mapping.playground.model
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class Ast3 extends AnyFlatSpec with Matchers {

  it should "identify approximately timy" in {
    val data = model.Person("I am not Timy!").toGenericContainer

    Contains(StringField("name"), "Timy")
      .eval(data) shouldBe true
  }

  it should "identify an apple" in {
    val data = model.ShoppingList(List(
      "banana",
      "apple"
    )).toGenericContainer

    Contains(ListField[String]("items"), "apple")
      .eval(data) shouldBe true
  }

  it should "identify the number 1" in {
    val data = model.NumberList(List(0, 1)).toGenericContainer

    Contains(ListField[Int]("numbers"), 1)
      .eval(data) shouldBe true
  }
}
