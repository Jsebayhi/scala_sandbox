package sandbox.dependency.playground.avro.interpreter.ast2.behavior

trait StartsWithable {
  def doesStartsWith(fieldContent: AnyRef): Boolean
}