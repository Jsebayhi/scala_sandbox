package sandbox.dependency.playground.avro.interpreter.ast6.accessor

import org.apache.avro.generic.GenericRecord

trait Accessor {
  def access(container: GenericRecord): AnyRef
}