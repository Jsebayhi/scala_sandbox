
package sandbox.scalapl.interpreter.playground.astB1

import org.apache.avro.generic.GenericRecord
import org.apache.avro.Schema
import scala.util.{ Failure, Success, Try }

object GenericRecordAccessor {

  def accessNestedField(fieldSequence: Seq[String], record: GenericRecord): Try[ObjectWithType] = {
    fieldSequence
      .foldLeft[Try[ObjectWithType]](
        Success(ObjectWithType(record, Schema.Type.RECORD))
      ) {
          case (Success(ObjectWithType(freshObject, _)), freshField) =>
            castToGenericRecord(freshObject).flatMap(r => accessField(freshField, r))
          case (failure, _) =>
            failure
        }
  }

  def accessField(fieldName: String, record: GenericRecord): Try[ObjectWithType] = {
    val obj = Option(record.get(fieldName))
    val schemaType = Option(record.getSchema.getField(fieldName)).map(f => f.schema.getType)

    obj.flatMap(o => schemaType.map(t => Success(ObjectWithType(o, t)))).getOrElse(
      Failure(UnknownFieldException(s"field $fieldName is not valid for schema ${record.getSchema.toString}"))
    )
  }

  def castToGenericRecord(o: Object): Try[GenericRecord] =
    Try(o.asInstanceOf[GenericRecord]).recoverWith {
      case e: Exception => Failure(NotCastableToGenericRecordException(o.toString, e))
    }
}

//TODO: TypedObject
case class ObjectWithType(obj: Object, typ: Schema.Type)

case class NotCastableToGenericRecordException(msg: String, cause: Exception = null) extends RuntimeException(msg, cause)
case class UnknownFieldException(msg: String, cause: Exception = null) extends RuntimeException(msg, cause)

