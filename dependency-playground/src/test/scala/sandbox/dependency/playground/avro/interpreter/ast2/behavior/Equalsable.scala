package sandbox.dependency.playground.avro.interpreter.ast2.behavior

trait Equalsable {
  def isEqual(fieldContent: AnyRef): Boolean
}
