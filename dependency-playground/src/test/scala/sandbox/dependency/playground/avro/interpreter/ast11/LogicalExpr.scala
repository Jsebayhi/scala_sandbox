package sandbox.scalapl.interpreter.playground.ast11

import org.apache.avro.generic.GenericRecord

trait LogicalExpression extends Expression

case class Not(expr: Expression) extends LogicalExpression {
  override def eval(container: GenericRecord): Boolean = {
    !expr.eval(container)
  }
}

case class And(left: Expression, right: Expression) extends LogicalExpression {
  override def eval(container: GenericRecord): Boolean = {
    left.eval(container) && right.eval(container)
  }
}

case class Or(left: Expression, right: Expression) extends LogicalExpression {
  override def eval(container: GenericRecord): Boolean = {
    left.eval(container) || right.eval(container)
  }
}
