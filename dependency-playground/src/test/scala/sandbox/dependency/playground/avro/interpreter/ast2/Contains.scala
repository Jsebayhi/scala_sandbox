package sandbox.dependency.playground.avro.interpreter.ast2

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import behavior.Containsable

case class Contains(field: String, containable: Containsable) extends Expression {
  override def eval(container: GenericContainer[AnyRef]): Boolean = {
    containable.isContained(container.get(field)
      .getOrElse(throw new AssertionError(s"incompatible schema, the field $field is unknown to this schema (${container.schema()}"))
    )
  }
}
