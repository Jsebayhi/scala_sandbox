package sandbox.dependency.playground.avro.interpreter.ast11

import org.apache.avro.generic.GenericRecord
import promise._

trait Predicate extends Expression

case class Contains[T](c: Containsable[T], v: Primitive[T]) extends Predicate {
  override def eval(container: GenericRecord): Boolean = {
    c.doesContain(container, v)
  }
}

case class Equals[T](e: Equalsable[T], v: Primitive[T]) extends Predicate {
  override def eval(container: GenericRecord): Boolean = {
    e.isEqual(container, v)
  }
}

case class StartsWith[T](s: StartsWithable[T], v: Primitive[T]) extends Predicate {
  override def eval(container: GenericRecord): Boolean = {
    s.doesStartsWith(container, v)
  }
}