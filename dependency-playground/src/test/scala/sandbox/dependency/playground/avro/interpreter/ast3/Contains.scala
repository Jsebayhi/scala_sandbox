package sandbox.dependency.playground.avro.interpreter.ast3

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import behavior.Containsable

case class Contains[T](fieldAccessor: Containsable[T], v: T) extends Expression {
  override def eval(container: GenericContainer[AnyRef]): Boolean = {
    fieldAccessor.doesContain(container, v)
  }
}
