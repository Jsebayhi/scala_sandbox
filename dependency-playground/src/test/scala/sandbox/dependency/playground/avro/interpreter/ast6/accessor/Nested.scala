package sandbox.dependency.playground.avro.interpreter.ast6.accessor

import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Try }

case class Nested(path: String) extends Accessor {

  private def castToGenericContainer(a: AnyRef): GenericRecord = Try(a.asInstanceOf[GenericRecord])
    .recoverWith { case e => Failure(new RuntimeException("given object is not a GenericRecord", e)) }
    .get

  override def access(container: GenericRecord): AnyRef = {
    val fieldsAccessor = path.split('.')
      .map(field => Direct(field))

    fieldsAccessor.drop(1)
      .foldLeft(fieldsAccessor(0).access(container)) {
        case (last, nextFieldAccessor) =>
          nextFieldAccessor.access(castToGenericContainer(last))
      }
  }
}
