package sandbox.dependency.playground.avro.interpreter.ast10

import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Success, Try }

object GenericRecordFieldAccessor {

  //TODO: test
  // - valid simple
  // - invalid simple, unknown field
  // - valid nested
  // - invalid nested, not a nested record
  // - invalid nested, unknown field
  def accessNestedField(fieldSequence: Seq[String], record: GenericRecord): Try[ObjectWithType] = {
    fieldSequence
      .foldLeft[Try[AccessStatus]](
        Success(AccessStatus(Seq.empty[String], ObjectWithType(record, Schema.Type.RECORD)))
      ) {
          case (Success(AccessStatus(done, ObjectWithType(freshObject, _))), nextField) =>
            castToGenericRecord(freshObject)
              .flatMap(r => accessField(nextField, r))
              .map(o => AccessStatus(done :+ nextField, o))
              .recoverWith { case e: Exception => Failure(InvalidPathException(done.mkString(".") + nextField, e)) }
          case (failure, _) => failure
        }.map(_.objectWithType)
  }

  def accessField(fieldName: String, record: GenericRecord): Try[ObjectWithType] = {
    val obj = Option(record.get(fieldName))
    val schemaType = Option(record.getSchema.getField(fieldName)).map(f => f.schema.getType)

    obj.flatMap(o => schemaType.map(t => Success(ObjectWithType(o, t)))).getOrElse(
      Failure(UnknownFieldException(fieldName, record.getSchema.toString))
    )
  }

  def castToGenericRecord(o: Object): Try[GenericRecord] = {
    Try(o.asInstanceOf[GenericRecord])
      .recoverWith {
        case e: Exception => Failure(NotCastableToGenericRecordException(o.toString, e))
      }
  }
}
case class ObjectWithType(obj: Object, typ: Schema.Type)

/**
 *
 * @param accessedPath Things that have been accessed. Used for error context.
 * @param objectWithType The last extracted object from the records.
 */
case class AccessStatus(accessedPath: Seq[String], objectWithType: ObjectWithType)

abstract class AccessorException(msg: String, e: Exception = null) extends RuntimeException(msg, e)

case class NotCastableToGenericRecordException(o: Object, e: Exception = null)
  extends AccessorException(s"invalid object, not a Generic Record. Object was $o.", e)

case class UnknownFieldException(field: String, schema: String)
  extends AccessorException(s"field $field not found in the schema $schema.")

case class InvalidPathException(path: String, e: Exception = null)
  extends AccessorException(s"invalid path '$path'", e)
