package sandbox.dependency.playground.avro.interpreter.ast.example

import com.carrefour.phenix.tradeitem.mapping.playground.ast.{ Expression, StringContains }
import com.carrefour.phenix.tradeitem.mapping.playground.model
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class Ast extends AnyFlatSpec with Matchers {

  it should "run" in {
    val name = "timy"
    val data = model.Person(name).toGenericContainer

    StringContains(field = "name", prim = name)
      .eval(data) shouldBe true
  }
}
