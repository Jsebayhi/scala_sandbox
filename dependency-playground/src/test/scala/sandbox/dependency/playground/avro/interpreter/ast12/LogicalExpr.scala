package sandbox.dependency.playground.avro.interpreter.ast12

import org.apache.avro.generic.GenericRecord

import scala.util.Try

trait LogicalExpression extends Expression

case class Not(expr: Expression) extends LogicalExpression {
  override def eval(container: GenericRecord): Try[Boolean] = {
    expr.eval(container).map(res => !res)
  }
}

case class And(left: Expression, right: Expression) extends LogicalExpression {
  override def eval(container: GenericRecord): Try[Boolean] = {
    left.eval(container).flatMap(lr => right.eval(container).map(_ && lr))
  }
}

case class Or(left: Expression, right: Expression) extends LogicalExpression {
  override def eval(container: GenericRecord): Try[Boolean] = {
    left.eval(container).flatMap(lr => right.eval(container).map(_ || lr))
  }
}
