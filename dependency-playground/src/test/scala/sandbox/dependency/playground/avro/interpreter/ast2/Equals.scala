package sandbox.dependency.playground.avro.interpreter.ast2

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import behavior.Equalsable

case class Equals(field: String, equalsable: Equalsable) extends Expression {
  override def eval(container: GenericContainer[AnyRef]): Boolean = {
    equalsable.isEqual(container.get(field)
      .getOrElse(throw new AssertionError(s"incompatible schema, the field $field is unknown to this schema (${container.schema()}"))
    )
  }
}
