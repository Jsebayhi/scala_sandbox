package sandbox.dependency.playground.avro.interpreter.ast6.accessor

import org.apache.avro.generic.GenericRecord

case class Direct(fieldName: String) extends Accessor {

  override def access(container: GenericRecord): AnyRef = {
    Option(container.get(fieldName))
      .getOrElse(throw new RuntimeException(s"incompatible schema, the field $fieldName is unknown to this schema (${container.getSchema.toString()}"))
  }
}
