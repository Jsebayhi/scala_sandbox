package sandbox.dependency.playground.avro.interpreter.ast6.field

import com.carrefour.phenix.tradeitem.mapping.playground.ast6.behavior._
import com.carrefour.phenix.tradeitem.mapping.playground.ast6.accessor.Accessor
import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Try }

case class StringField(accessor: Accessor) extends Containsable[String]
  with Equalsable[String]
  with StartsWithable[String] {

  private def cast(a: AnyRef): String = Try(a.asInstanceOf[String])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a String", e)) }
    .get

  override def doesContain(container: GenericRecord, candidate: String): Boolean = {
    cast(accessor.access(container)).contains(candidate)
  }

  override def isEqual(container: GenericRecord, candidate: String): Boolean = {
    candidate == cast(accessor.access(container))
  }

  override def doesStartsWith(container: GenericRecord, candidate: String): Boolean = {
    cast(accessor.access(container)).startsWith(candidate)
  }
}
