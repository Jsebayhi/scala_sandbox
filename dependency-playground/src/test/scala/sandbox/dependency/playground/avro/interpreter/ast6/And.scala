package sandbox.scalapl.interpreter.playground.ast6

import org.apache.avro.generic.GenericRecord

case class And(left: Expression, right: Expression) extends Expression {

  override def eval(container: GenericRecord): Boolean = {
    left.eval(container) && right.eval(container)
  }

}
