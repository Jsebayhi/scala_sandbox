package sandbox.scalapl.interpreter.playground.ast3

import com.carrefour.phenix.tradeitem.mapping.playground.AnyRefContainer.AnyRefContainer

case class Or(left: Expression, right: Expression) extends Expression {

  override def eval(container: AnyRefContainer): Boolean = {
    left.eval(container) || right.eval(container)
  }

}
