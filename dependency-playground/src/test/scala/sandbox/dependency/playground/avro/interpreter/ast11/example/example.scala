package sandbox.dependency.playground.avro.interpreter.ast11.example

import com.carrefour.phenix.tradeitem.mapping.playground.ast11._
import com.carrefour.phenix.tradeitem.mapping.playground.model
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class ast11Example extends AnyFlatSpec with Matchers {

  it should "identify approximately timy" in {
    val data = model.Person("I am not Timy!").toGenericAvro

    Contains(PromisedStringPrim(Seq("name")), StringPrim("Timy"))
      .eval(data) shouldBe true
  }

  it should "identify timy" in {
    val data = model.Person("Timy").toGenericAvro

    Equals(PromisedStringPrim(Seq("name")), StringPrim("Timy"))
      .eval(data) shouldBe true
  }

  it should "identify not timy" in {
    val data = model.Person("I am not Timy!").toGenericAvro

    Not(Equals(PromisedStringPrim(Seq("name")), StringPrim("Timy")))
      .eval(data) shouldBe true
  }

  it should "identify that timy starts with 'tim'" in {
    val data = model.Person("Timy").toGenericAvro

    StartsWith(PromisedStringPrim(Seq("name")), StringPrim("Tim"))
      .eval(data) shouldBe true
  }

  it should "identify an apple" in {
    val data = model.ShoppingList(List(
      "banana",
      "apple"
    )).toGenericAvro

    Contains(PromisedListPrim[String](Seq("items")), StringPrim("apple"))
      .eval(data) shouldBe true
  }

  it should "identify the number 1" in {
    val data = model.NumberList(List(0, 1)).toGenericAvro

    Contains(PromisedListPrim[Int](Seq("numbers")), IntPrim(1))
      .eval(data) shouldBe true
  }

  it should "handle nested" in {
    val data = model.Nested(model.Person("toto")).toGenericAvro

    Contains(PromisedStringPrim(Seq("person", "name")), StringPrim("toto"))
      .eval(data) shouldBe true

  }
}
