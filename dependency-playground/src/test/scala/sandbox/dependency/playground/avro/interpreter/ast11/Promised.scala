package sandbox.scalapl.interpreter.playground.ast11

import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Try }

import promise._

trait Promised[T]

case class PromisedListPrim[T](path: Seq[String]) extends Promised[List[T]]
  with Containsable[T] {

  private def cast(o: ObjectWithType): Try[ListPrim[T]] = {
    o.typ match {
      case Schema.Type.ARRAY => Try(ListPrim[T](o.obj.asInstanceOf[List[T]])) //TODO: handle the mapping of the type in the array
        .recoverWith { case e: Exception => Failure(TypeMismatchException(o.typ.getName, "List[T]", e)) }
      case _ => Failure(TypeMismatchException(o.typ.getName, "List[T]"))
    }
  }

  override def doesContain(record: GenericRecord, candidate: Primitive[T]): Boolean = {
    GenericRecordFieldAccessor.accessNestedField(path, record)
      .flatMap(lst => cast(lst))
      .map(_.doesContain(candidate))
      .get
  }
}

case class PromisedStringPrim(path: Seq[String]) extends Promised[String]
  with Containsable[String]
  with Equalsable[String]
  with StartsWithable[String] {

  override def doesContain(record: GenericRecord, candidate: Primitive[String]): Boolean = {
    accessAndCast(record)
      .map(_.doesContain(candidate))
      .get
  }

  override def isEqual(record: GenericRecord, candidate: Primitive[String]): Boolean = {
    accessAndCast(record)
      .map(_.isEqual(candidate))
      .get
  }

  override def doesStartsWith(record: GenericRecord, candidate: Primitive[String]): Boolean = {
    accessAndCast(record)
      .map(_.doesStartsWith(candidate))
      .get
  }

  private def accessAndCast(record: GenericRecord): Try[StringPrim] = {
    GenericRecordFieldAccessor.accessNestedField(path, record)
      .flatMap(lst => cast(lst))
  }

  private def cast(o: ObjectWithType): Try[StringPrim] = {
    o.typ match {
      case Schema.Type.STRING => Try(StringPrim(o.obj.asInstanceOf[String]))
        .recoverWith { case e: Exception => Failure(TypeMismatchException(o.typ.getName, "String", e)) }
      case _ => Failure(TypeMismatchException(o.typ.getName, "String"))
    }
  }
}

case class PromisedIntPrim(path: Seq[String]) extends Promised[Int]
  with Equalsable[Int] {

  private def cast(o: ObjectWithType): Try[IntPrim] = {
    o.typ match {
      case Schema.Type.INT => Try(IntPrim(o.obj.asInstanceOf[Int]))
        .recoverWith { case e: Exception => Failure(TypeMismatchException(o.typ.getName, "Int", e)) }
      case _ => Failure(TypeMismatchException(o.typ.getName, "Int"))
    }
  }

  override def isEqual(record: GenericRecord, candidate: Primitive[Int]): Boolean = {
    GenericRecordFieldAccessor.accessNestedField(path, record)
      .flatMap(lst => cast(lst))
      .map(_.isEqual(candidate))
      .get
  }
}

abstract class PromisedException(msg: String, cause: Exception = null) extends RuntimeException(msg, cause)

case class TypeMismatchException(thing: Object, expectedType: String, cause: Exception = null)
  extends PromisedException(s"type mismatch, $thing is not a $expectedType.", cause)