package sandbox.dependency.playground.avro.interpreter.ast12.primitive

import com.carrefour.phenix.tradeitem.mapping.playground.ast12._

trait StringPrimContainsable {
  def doesContain(candidate: StringPrim): Boolean
}

trait StringPrimEqualsable {
  def isEqual(fieldContent: StringPrim): Boolean
}

trait StringPrimStartsWithable {
  def doesStartsWith(fieldContent: StringPrim): Boolean
}

trait IntPrimEqualsable {
  def isEqual(candidate: IntPrim): Boolean
}