package sandbox.dependency.playground.avro.interpreter.ast6

import behavior.Containsable
import org.apache.avro.generic.GenericRecord

case class Contains[T](fieldAccessor: Containsable[T], v: T) extends Expression {
  override def eval(container: GenericRecord): Boolean = {
    fieldAccessor.doesContain(container, v)
  }
}
