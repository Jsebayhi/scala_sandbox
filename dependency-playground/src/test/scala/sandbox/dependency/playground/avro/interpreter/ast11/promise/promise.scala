package sandbox.dependency.playground.avro.interpreter.ast11.promise

import com.carrefour.phenix.tradeitem.mapping.playground.ast11.Primitive
import org.apache.avro.generic.GenericRecord

trait Containsable[T] {
  def doesContain(container: GenericRecord, candidate: Primitive[T]): Boolean
}

trait Equalsable[T] {
  def isEqual(container: GenericRecord, fieldContent: Primitive[T]): Boolean
}

trait StartsWithable[T] {
  def doesStartsWith(container: GenericRecord, fieldContent: Primitive[T]): Boolean
}