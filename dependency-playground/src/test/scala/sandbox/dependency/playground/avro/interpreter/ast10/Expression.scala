package sandbox.dependency.playground.avro.interpreter.ast10

import org.apache.avro.generic.GenericRecord

trait Expression {
  def eval(container: GenericRecord): Boolean
}