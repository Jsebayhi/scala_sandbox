
package sandbox.dependency.playground.avro.interpreter.astB2

import com.carrefour.phenix.tradeitem.mapping.playground.astB2.GenericRecordAccessor._
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Success, Try }

trait Interpreter {
  def eval(ast: RuleFilterAST)(record: GenericRecord): Try[Boolean]
}

//TODO: add error context with recoverWith{case e => Failure(MoreContext(e))}
object InterpreterV0 extends Interpreter {

  override def eval(ast: RuleFilterAST)(record: GenericRecord): Try[Boolean] = evalLogicalExpr(ast.expr)(record)

  def evalLogicalExpr(expr: LogicalExpr)(record: GenericRecord): Try[Boolean] = expr match {
    case OrExpr(l, r)  => evalLogicalExpr(l)(record).flatMap(lr => evalLogicalExpr(r)(record).map(_ || lr))
    case AndExpr(l, r) => evalLogicalExpr(l)(record).flatMap(lr => evalLogicalExpr(r)(record).map(_ && lr))
    case NotExpr(e)    => evalLogicalExpr(e)(record).map(res => !res)
    case PredExpr(p)   => evalPredicate(p)(record)
  }

  def evalPredicate(predicate: Predicate)(record: GenericRecord): Try[Boolean] = predicate match {
    case Contains(promise, argument)   => evalPromise(promise, record).flatMap(evalContains(_, argument))
    case Equals(promise, argument)     => evalPromise(promise, record).flatMap(evalEquals(_, argument))
    case StartsWith(promise, argument) => evalPromise(promise, record).flatMap(evalStartWith(_, argument))
  }

  def evalPromise(promise: Promise, record: GenericRecord): Try[Primitive] = promise match {
    case StringPromise(path) => extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.STRING => Success(StringPrim(obj.asInstanceOf[String].trim))
    }
    case IntPromise(path) => extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.INT => Success(IntPrim(obj.asInstanceOf[Int]))
    }
    case ArrayStringPromise(path) => extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.ARRAY => Success(ArrayStringPrim(obj.asInstanceOf[List[String]].map(_.trim)))
    }
  }

  //Fix: need to run it to detect bad primitive error
  def evalContains(fieldContent: Primitive, argument: Primitive): Try[Boolean] = (fieldContent, argument) match {
    case (StringPrim(s1), StringPrim(s2))    => Success(s1.contains(s2))
    case (ArrayStringPrim(l), StringPrim(s)) => Success(l.contains(s))
    case _                                   => Failure(BadPrimitive())
  }

  //Fix: need to run it to detect bad primitive error
  def evalEquals(fieldContent: Primitive, argument: Primitive): Try[Boolean] = (fieldContent, argument) match {
    case (StringPrim(s1), StringPrim(s2)) => Success(s1.equals(s2))
    case (IntPrim(i1), IntPrim(i2))       => Success(i1 == i2)
    case (StringPrim(s), IntPrim(i))      => Success(s == i.toString) //TODO: do we keep me?
    case (IntPrim(i), StringPrim(s))      => Success(i.toString == s) //TODO: do we keep me?
    case _                                => Failure(BadPrimitive())
  }

  //Fix: need to run it to detect bad primitive error
  def evalStartWith(fieldContent: Primitive, argument: Primitive): Try[Boolean] = (fieldContent, argument) match {
    case (StringPrim(s1), StringPrim(s2)) => Success(s1.startsWith(s2))
    case _                                => Failure(BadPrimitive())
  }

}

abstract class PredicateEvaluationException(msg: String, cause: Exception = null) extends RuntimeException(msg, cause)
case class BadPrimitive() extends PredicateEvaluationException("bad primitive exception")
