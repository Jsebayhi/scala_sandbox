package sandbox.dependency.playground.avro.interpreter.ast6

import org.apache.avro.generic.GenericRecord

case class Not(expr: Expression) extends Expression {
  override def eval(container: GenericRecord): Boolean = {
    !expr.eval(container)
  }
}