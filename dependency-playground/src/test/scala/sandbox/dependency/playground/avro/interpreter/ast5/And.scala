package sandbox.dependency.playground.avro.interpreter.ast5

import com.carrefour.phenix.tradeitem.mapping.playground.AnyRefContainer.AnyRefContainer

case class And(left: Expression, right: Expression) extends Expression {

  override def eval(container: AnyRefContainer): Boolean = {
    left.eval(container) && right.eval(container)
  }

}
