package sandbox.scalapl.interpreter.playground.ast5

import com.carrefour.phenix.tradeitem.mapping.playground.AnyRefContainer.AnyRefContainer

trait Expression {
  def eval(container: AnyRefContainer): Boolean
}