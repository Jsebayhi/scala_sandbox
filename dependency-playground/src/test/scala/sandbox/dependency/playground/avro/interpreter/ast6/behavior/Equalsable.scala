package sandbox.scalapl.interpreter.playground.ast6.behavior

import org.apache.avro.generic.GenericRecord

trait Equalsable[T] {
  def isEqual(container: GenericRecord, fieldContent: T): Boolean
}
