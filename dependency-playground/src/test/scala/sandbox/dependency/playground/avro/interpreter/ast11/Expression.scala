package sandbox.scalapl.interpreter.playground.ast11

import org.apache.avro.generic.GenericRecord

trait Expression {
  def eval(container: GenericRecord): Boolean
}