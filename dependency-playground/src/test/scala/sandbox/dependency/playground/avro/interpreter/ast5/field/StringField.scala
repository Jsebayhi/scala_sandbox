package sandbox.dependency.playground.avro.interpreter.ast5.field

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.behavior._
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.accessor.Accessor

import scala.util.{ Failure, Try }

case class StringField(accessor: Accessor) extends Containsable[String]
  with Equalsable[String]
  with StartsWithable[String] {

  private def cast(a: AnyRef): String = Try(a.asInstanceOf[String])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a String", e)) }
    .get

  override def doesContain(container: GenericContainer[AnyRef], candidate: String): Boolean = {
    cast(accessor.access(container)).contains(candidate)
  }

  override def isEqual(container: GenericContainer[AnyRef], candidate: String): Boolean = {
    candidate == cast(accessor.access(container))
  }

  override def doesStartsWith(container: GenericContainer[AnyRef], candidate: String): Boolean = {
    cast(accessor.access(container)).startsWith(candidate)
  }
}
