
package sandbox.dependency.playground.avro.interpreter.astB3

import com.carrefour.phenix.tradeitem.mapping.playground.model._
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Success

class Example extends AnyFlatSpec with Matchers {

  val tony = Person("Tony").toGenericAvro
  val nestedTony = Nested(Person("Tony")).toGenericAvro
  val shoppingList = ShoppingList(items = List("apple", "хлеб", "картофель")).toGenericAvro
  val id1 = IntId(1).toGenericAvro

  val findTony = RuleFilterAST(
    PredExpr(
      ContainsString(StringPromise(Seq("name")), StringPrim("Tony"))
    )
  )

  val findNestedTony = RuleFilterAST(
    PredExpr(
      ContainsString(StringPromise(Seq("person", "name")), StringPrim("Tony"))
    )
  )

  val findNotTony = RuleFilterAST(
    NotExpr(PredExpr(
      ContainsString(StringPromise(Seq("name")), StringPrim("Tony"))
    ))
  )

  val findAppleInTheShoppingList = RuleFilterAST(
    PredExpr(
      ContainsString(ArrayStringPromise(Seq("items")), StringPrim("apple"))
    )
  )

  val findId1 = RuleFilterAST(
    PredExpr(
      EqualsInt(IntPromise(Seq("id")), IntPrim(1))
    )
  )

  it should "find nested tony" in {
    val res = InterpreterV0.eval(findNestedTony)(nestedTony)
    res shouldBe Success(true)
  }

  it should "find tony" in {
    val res = InterpreterV0.eval(findTony)(tony)
    res shouldBe Success(true)
  }

  it should "find the apple in the shopping list" in {
    val res = InterpreterV0.eval(findAppleInTheShoppingList)(shoppingList)
    res shouldBe Success(true)
  }

  it should "find the correct id" in {
    val res = InterpreterV0.eval(findId1)(id1)
    res shouldBe Success(true)
  }

  it should s"""find an id of "1"""" in {
    val data = StringId("1").toGenericAvro

    val rule = RuleFilterAST(
      PredExpr(
        EqualsInt(StringPromise(Seq("id")), IntPrim(1))
      )
    )

    InterpreterV0.eval(rule)(data)
  }

  it should s"""find an id of String "1"""" in {
    val data = IntId(1).toGenericAvro

    val rule = RuleFilterAST(
      PredExpr(
        EqualsInt(IntPromise(Seq("id")), IntPrim(1))
      )
    )

    InterpreterV0.eval(rule)(data)
  }

  it should s"""find an idString of "1"""" in {
    val data = StringId("1").toGenericAvro

    val rule = RuleFilterAST(
      PredExpr(
        EqualsString(StringPromise(Seq("id")), StringPrim("1"))
      )
    )

    InterpreterV0.eval(rule)(data)
  }

  it should s"""find an idString of String 1""" in {
    val data = IntId(1).toGenericAvro

    val rule = RuleFilterAST(
      PredExpr(
        EqualsString(IntPromise(Seq("id")), StringPrim("1"))
      )
    )

    InterpreterV0.eval(rule)(data)
  }
}
