package sandbox.dependency.playground.avro.interpreter.model

import com.carrefour.phenix.tradeitem.mapping.playground.{ AnyRefContainer, GenericContainer }
import org.apache.avro.Schema
import org.apache.avro.generic.GenericData.Record

case class Nested(person: Person) {
  def toGenericContainer: GenericContainer[AnyRef] = {
    AnyRefContainer().set("person", person.toGenericContainer)
  }

  val schema =
    s"""
       |{
       |  "name": "${this.getClass.getSimpleName}",
       |  "type": "record",
       |  "fields": [
       |    {
       |      "name": "person",
       |      "type": ${person.schema}
       |    }
       |  ]
       |}
    """.stripMargin

  val avroSchema = new Schema.Parser().parse(schema)

  def toGenericAvro: Record = {
    val r = new Record(avroSchema)
    r.put("person", person.toGenericAvro)
    r
  }
}
