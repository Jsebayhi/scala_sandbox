package sandbox.dependency.playground.avro.interpreter.ast6.field

import com.carrefour.phenix.tradeitem.mapping.playground.ast6.behavior.Containsable
import com.carrefour.phenix.tradeitem.mapping.playground.ast6.accessor.Accessor
import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Try }

case class ListField[T](accessor: Accessor) extends Containsable[T] {

  private def cast(a: AnyRef): List[T] = Try(a.asInstanceOf[List[T]])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a List", e)) }
    .get

  override def doesContain(container: GenericRecord, candidate: T): Boolean = {
    cast(accessor.access(container)).contains(candidate)
  }
}
