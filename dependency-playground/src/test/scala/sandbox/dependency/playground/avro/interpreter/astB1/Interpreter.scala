
package sandbox.dependency.playground.avro.interpreter.astB1

import org.apache.avro.generic.GenericRecord
import org.apache.avro.Schema
import GenericRecordAccessor._
import scala.util.{ Failure, Success, Try }

trait Interpreter {
  def eval(ast: RuleFilterAST, record: GenericRecord): Boolean
}

object InterpreterV0 extends Interpreter {

  override def eval(ast: RuleFilterAST, record: GenericRecord) = evalLogicalExpr(ast.expr, record)

  def evalLogicalExpr(expr: LogicalExpr, record: GenericRecord): Boolean = expr match {
    case OrExpr(l, r)  => evalLogicalExpr(l, record) || evalLogicalExpr(r, record)
    case AndExpr(l, r) => evalLogicalExpr(l, record) && evalLogicalExpr(r, record)
    case NotExpr(e)    => !evalLogicalExpr(e, record)
    case PredExpr(p)   => evalPredicate(p, record)
  }

  def evalPredicate(predicate: Predicate, record: GenericRecord): Boolean = predicate match {
    case Contains(fieldSequence, argument) =>
      evalContains(primitiveFromRecordFieldSequence(fieldSequence, record).get, argument)
    case Equals(fieldSequence, argument) =>
      evalEquals(primitiveFromRecordFieldSequence(fieldSequence, record).get, argument)
    case StartsWith(fieldSequence, argument) =>
      evalStartWith(primitiveFromRecordFieldSequence(fieldSequence, record).get, argument)
  }

  def evalContains(fieldContent: Primitive, argument: Primitive): Boolean = (fieldContent, argument) match {
    case (StringPrim(s1), StringPrim(s2))    => s1.contains(s2)
    case (ArrayStringPrim(l), StringPrim(s)) => l.contains(s)
    case _                                   => ??? // Throw Exception
  }

  def evalEquals(fieldContent: Primitive, argument: Primitive): Boolean = (fieldContent, argument) match {
    case (StringPrim(s1), StringPrim(s2)) => s1.equals(s2)
    case _                                => ??? // Throw Exception
  }

  def evalStartWith(fieldContent: Primitive, argument: Primitive): Boolean = (fieldContent, argument) match {
    case (StringPrim(s1), StringPrim(s2)) => s1.equals(s2)
    case _                                => ??? // Throw Exception
  }

  // FIX: This way of doing does not enforce the extracted type from the record compared to the expected type from the trade item schema
  // If we have a bad data with a different schema as input, we won't see it.
  def primitiveFromRecordFieldSequence(fieldSequence: Seq[String], record: GenericRecord): Try[Primitive] = {
    accessNestedField(fieldSequence, record).flatMap {
      case ObjectWithType(obj, typ) => typ match {
        case Schema.Type.STRING => Success(StringPrim(obj.asInstanceOf[String]))
        case Schema.Type.INT    => Success(StringPrim(obj.asInstanceOf[Int].toString))
        case t                  => Failure(FieldTypeNotHandledException(t.toString))
      }
    }
  }

}

case class FieldTypeNotHandledException(msg: String, cause: Exception = null) extends RuntimeException(msg, cause)
