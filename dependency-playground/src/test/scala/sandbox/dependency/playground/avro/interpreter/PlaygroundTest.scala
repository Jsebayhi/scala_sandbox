package sandbox.dependency.playground.avro.interpreter

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class PlaygroundTest extends AnyFlatSpec with Matchers {

  it should "do something" in {
    model.Person("toto").toGenericAvro.get("famillyName") shouldBe null
  }

}
