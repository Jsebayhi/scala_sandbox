package sandbox.dependency.playground.avro.interpreter.ast4.accessor

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer

import scala.util.{ Failure, Try }

case class Nested(path: String) extends Accessor {

  private def castToGenericContainer(a: AnyRef): GenericContainer[AnyRef] = Try(a.asInstanceOf[GenericContainer[AnyRef]])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a GenericContainer[AnyRef]", e)) }
    .get

  override def access(container: GenericContainer[AnyRef]): AnyRef = {
    val fieldsAccessor = path.split('.')
      .map(field => Direct(field))

    fieldsAccessor.drop(1)
      .foldLeft(fieldsAccessor(0).access(container)) {
        case (last, nextFieldAccessor) =>
          nextFieldAccessor.access(castToGenericContainer(last))
      }
  }
}
