package sandbox.dependency.playground.avro.interpreter.ast11

import primitive._

trait Primitive[T] {
  def value: T
}

case class ListPrim[T](lst: List[T]) extends Primitive[List[T]]
  with Containsable[T] {

  override def value: List[T] = lst

  override def doesContain(candidate: Primitive[T]): Boolean = {
    lst.contains(candidate.value)
  }
}

case class StringPrim(str: String) extends Primitive[String]
  with Containsable[String]
  with Equalsable[String]
  with StartsWithable[String] {

  override def value: String = str

  override def doesContain(candidate: Primitive[String]): Boolean = {
    str.contains(candidate.value)
  }

  override def isEqual(candidate: Primitive[String]): Boolean = {
    str == candidate.value
  }

  override def doesStartsWith(candidate: Primitive[String]): Boolean = {
    str.startsWith(candidate.value)
  }
}

case class IntPrim(i: Int) extends Primitive[Int]
  with Equalsable[Int] {

  override def value: Int = i

  override def isEqual(candidate: Primitive[Int]): Boolean = {
    i == candidate.value
  }
}