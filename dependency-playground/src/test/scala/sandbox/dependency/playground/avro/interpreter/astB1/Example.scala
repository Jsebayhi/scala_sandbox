
package sandbox.dependency.playground.avro.interpreter.astB1

import com.carrefour.phenix.tradeitem.mapping.playground.model._

import org.scalatest.{ FlatSpec, GivenWhenThen, Matchers }

class Example extends AnyFlatSpec with Matchers {

  val nested = Nested(Person("Stark"))

  val ast = RuleFilterAST(
    OrExpr(
      PredExpr(
        Contains(Seq("person", "name"), StringPrim("Tony"))
      ),
      PredExpr(
        Equals(Seq("person", "name"), StringPrim("Stark"))
      )
    )
  )

  it should "interpret" in {
    assert(InterpreterV0.eval(ast, nested.toGenericAvro))
  }

}
