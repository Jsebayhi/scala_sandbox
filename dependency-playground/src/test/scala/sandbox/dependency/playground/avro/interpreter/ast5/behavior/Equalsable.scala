package sandbox.dependency.playground.avro.interpreter.ast5.behavior

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer

trait Equalsable[T] {
  def isEqual(container: GenericContainer[AnyRef], fieldContent: T): Boolean
}
