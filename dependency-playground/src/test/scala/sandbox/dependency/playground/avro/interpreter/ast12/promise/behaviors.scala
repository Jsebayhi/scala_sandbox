package sandbox.dependency.playground.avro.interpreter.ast12.promise

import com.carrefour.phenix.tradeitem.mapping.playground.ast12._
import org.apache.avro.generic.GenericRecord

import scala.util.Try

trait StringPrimContainsable {
  def doesContain(container: GenericRecord, candidate: StringPrim): Try[Boolean]
}

trait StringPrimEqualsable {
  def isEqual(container: GenericRecord, fieldContent: StringPrim): Try[Boolean]
}

trait StringPrimStartsWithable {
  def doesStartsWith(container: GenericRecord, fieldContent: StringPrim): Try[Boolean]
}

trait IntPrimEqualsable {
  def isEqual(container: GenericRecord, candidate: IntPrim): Try[Boolean]
}