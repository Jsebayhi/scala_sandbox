package sandbox.scalapl.interpreter.playground.ast4.field

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import com.carrefour.phenix.tradeitem.mapping.playground.ast4.behavior._
import com.carrefour.phenix.tradeitem.mapping.playground.ast4.accessor.Accessor

import scala.util.{ Failure, Try }

case class StringField(accessor: Accessor) extends Containsable[String] {

  private def cast(a: AnyRef): String = Try(a.asInstanceOf[String])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a String", e)) }
    .get

  override def doesContain(container: GenericContainer[AnyRef], candidate: String): Boolean = {
    cast(accessor.access(container)).contains(candidate)
  }
}
