package sandbox.dependency.playground.avro.interpreter.ast12

import org.apache.avro.generic.GenericRecord
import promise._

import scala.util.Try

trait Predicate extends Expression

case class ContainsString(c: StringPrimContainsable, v: StringPrim) extends Predicate {
  override def eval(container: GenericRecord): Try[Boolean] = {
    c.doesContain(container, v)
  }
}

case class EqualsString(e: StringPrimEqualsable, v: StringPrim) extends Predicate {
  override def eval(container: GenericRecord): Try[Boolean] = {
    e.isEqual(container, v)
  }
}

case class EqualsInt(e: IntPrimEqualsable, v: IntPrim) extends Predicate {
  override def eval(container: GenericRecord): Try[Boolean] = {
    e.isEqual(container, v)
  }
}

case class StartsWithString(s: StringPrimStartsWithable, v: StringPrim) extends Predicate {
  override def eval(container: GenericRecord): Try[Boolean] = {
    s.doesStartsWith(container, v)
  }
}