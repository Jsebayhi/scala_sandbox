package sandbox.dependency.playground.avro.interpreter.ast12

import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Try }
import promise._

trait Promised[T]

case class PromisedListStringPrim(path: Seq[String]) extends Promised[List[StringPrim]]
  with StringPrimContainsable {

  override def doesContain(record: GenericRecord, candidate: StringPrim): Try[Boolean] = {
    accessAndCast(record)
      .map(_.doesContain(candidate))
  }

  private def accessAndCast(record: GenericRecord): Try[ListStringPrim] = {
    GenericRecordFieldAccessor.extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.ARRAY => {
        Try(ListStringPrim(obj.asInstanceOf[List[String]].map(e => StringPrim(e.trim))))
      }
    }.recoverWith { case e: Exception => Failure(TypeMismatchException("Array[String]", e)) }
  }
}

case class PromisedStringPrim(path: Seq[String]) extends Promised[String]
  with StringPrimContainsable
  with StringPrimEqualsable
  with StringPrimStartsWithable
  with IntPrimEqualsable {

  override def doesContain(record: GenericRecord, candidate: StringPrim): Try[Boolean] = {
    accessAndCast(record)
      .map(_.doesContain(candidate))
  }

  override def isEqual(record: GenericRecord, candidate: StringPrim): Try[Boolean] = {
    accessAndCast(record)
      .map(_.isEqual(candidate))
  }

  override def doesStartsWith(record: GenericRecord, candidate: StringPrim): Try[Boolean] = {
    accessAndCast(record)
      .map(_.doesStartsWith(candidate))
  }

  override def isEqual(record: GenericRecord, candidate: IntPrim): Try[Boolean] = {
    accessAndCast(record)
      .map(_.isEqual(candidate))
  }

  private def accessAndCast(record: GenericRecord): Try[StringPrim] = {
    GenericRecordFieldAccessor.extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.STRING => {
        Try(StringPrim(obj.asInstanceOf[String]))
      }
    }.recoverWith { case e: Exception => Failure(TypeMismatchException("String", e)) }
  }
}

case class PromisedIntPrim(path: Seq[String]) extends Promised[Int]
  with IntPrimEqualsable
  with StringPrimEqualsable {

  override def isEqual(record: GenericRecord, candidate: IntPrim): Try[Boolean] = {
    accessAndCast(record)
      .map(_.isEqual(candidate))
  }

  override def isEqual(record: GenericRecord, candidate: StringPrim): Try[Boolean] = {
    accessAndCast(record)
      .map(_.isEqual(candidate))
  }

  private def accessAndCast(record: GenericRecord): Try[IntPrim] = {
    GenericRecordFieldAccessor.extractNestedField(path, record) {
      case ObjectWithType(obj, typ) if typ == Schema.Type.INT => {
        Try(IntPrim(obj.asInstanceOf[Int]))
      }
    }.recoverWith { case e: Exception => Failure(TypeMismatchException("Int", e)) }
  }
}

abstract class PromisedException(msg: String, cause: Exception = null) extends RuntimeException(msg, cause)

case class TypeMismatchException(expectedType: String, cause: Exception = null)
  extends PromisedException(s"type mismatch, expected $expectedType", cause)