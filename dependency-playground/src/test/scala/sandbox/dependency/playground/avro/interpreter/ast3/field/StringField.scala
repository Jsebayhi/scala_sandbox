package sandbox.dependency.playground.avro.interpreter.ast3.field

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import com.carrefour.phenix.tradeitem.mapping.playground.ast3.behavior._

import scala.util.{ Failure, Try }

case class StringField(fieldName: String) extends Containsable[String] {

  private def cast(a: AnyRef): String = Try(a.asInstanceOf[String])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a String", e)) }
    .get

  override def doesContain(container: GenericContainer[AnyRef], candidate: String): Boolean = {
    cast(container.get(fieldName)
      .getOrElse(throw new RuntimeException(s"incompatible schema, the field $fieldName is unknown to this schema (${container.schema()}"))
    ).contains(candidate)
  }
}
