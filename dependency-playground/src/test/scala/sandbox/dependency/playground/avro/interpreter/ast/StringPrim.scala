package sandbox.dependency.playground.avro.interpreter.ast

import com.carrefour.phenix.tradeitem.mapping.playground.AnyRefContainer.AnyRefContainer

import scala.util.{ Failure, Try }

object StringPrim {
  def getFieldValue(container: AnyRefContainer, field: String): String = Try {
    container.get(field)
      .get
      .asInstanceOf[String]
  }.recoverWith {
    case e => Failure(new AssertionError(s"bad schema, the field $field is unknown to this schema (${container.schema()}", e))
  }.get
}

case class StringContains[T](field: String, prim: String) extends Expression {
  override def eval(container: AnyRefContainer): Boolean = {
    StringPrim.getFieldValue(container, field).contains(prim)
  }
}

case class StringEquals[T](field: String, prim: String) extends Expression {
  override def eval(container: AnyRefContainer): Boolean = {
    prim == StringPrim.getFieldValue(container, field)
  }
}

case class StringStartsWith[T](field: String, prim: String) extends Expression {
  override def eval(container: AnyRefContainer): Boolean = {
    StringPrim.getFieldValue(container, field).startsWith(prim)
  }
}