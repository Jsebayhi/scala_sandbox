package sandbox.dependency.playground.avro.interpreter.ast2.example

import com.carrefour.phenix.tradeitem.mapping.playground.ast2._
import com.carrefour.phenix.tradeitem.mapping.playground.model
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

/**
 * Bad design. How do you do if you need to check if a list contains a String???
 */
class Ast2 extends AnyFlatSpec with Matchers {

  it should "identify timy" in {
    val name = "Timy"
    val data = model.Person(name).toGenericContainer

    Equals(field = "name", equalsable = StringPrim(name))
      .eval(data) shouldBe true
  }

  it should "identify not timy" in {
    val data = model.Person("I am not Timy!!").toGenericContainer

    Not(Equals(field = "name", equalsable = StringPrim("Timy")))
      .eval(data) shouldBe true
  }

  it should "identify approximately timy" in {
    val data = model.Person("I am not Timy!").toGenericContainer

    Contains(field = "name", containable = StringPrim("Timy"))
      .eval(data) shouldBe true
  }
}