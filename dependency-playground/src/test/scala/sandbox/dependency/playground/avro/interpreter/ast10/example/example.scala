package sandbox.dependency.playground.avro.interpreter.ast10.example

import com.carrefour.phenix.tradeitem.mapping.playground.ast10._
import com.carrefour.phenix.tradeitem.mapping.playground.model
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class ast10Example extends AnyFlatSpec with Matchers {

  it should "identify approximately timy" in {
    val data = model.Person("I am not Timy!").toGenericAvro

    Contains(PromisedString(Seq("name")), "Timy")
      .eval(data) shouldBe true
  }

  it should "identify timy" in {
    val data = model.Person("Timy").toGenericAvro

    Equals(PromisedString(Seq("name")), "Timy")
      .eval(data) shouldBe true
  }

  it should "identify not timy" in {
    val data = model.Person("I am not Timy!").toGenericAvro

    Not(Equals(PromisedString(Seq("name")), "Timy"))
      .eval(data) shouldBe true
  }

  it should "identify that timy starts with 'tim'" in {
    val data = model.Person("Timy").toGenericAvro

    StartsWith(PromisedString(Seq("name")), "Tim")
      .eval(data) shouldBe true
  }

  it should "identify an apple" in {
    val data = model.ShoppingList(List(
      "banana",
      "apple"
    )).toGenericAvro

    Contains(PromisedList[String](Seq("items")), "apple")
      .eval(data) shouldBe true
  }

  it should "identify the number 1" in {
    val data = model.NumberList(List(0, 1)).toGenericAvro

    Contains(PromisedList[Int](Seq("numbers")), 1)
      .eval(data) shouldBe true
  }

  it should "handle nested" in {
    val data = model.Nested(model.Person("toto")).toGenericAvro

    Contains(PromisedString(Seq("person", "name")), "toto")
      .eval(data) shouldBe true

  }
}
