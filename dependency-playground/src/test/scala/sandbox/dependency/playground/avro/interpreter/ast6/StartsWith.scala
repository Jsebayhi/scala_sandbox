package sandbox.dependency.playground.avro.interpreter.ast6

import com.carrefour.phenix.tradeitem.mapping.playground.ast6.behavior.StartsWithable
import org.apache.avro.generic.GenericRecord

case class StartsWith[T](fieldAccessor: StartsWithable[T], v: T) extends Expression {
  override def eval(container: GenericRecord): Boolean = {
    fieldAccessor.doesStartsWith(container, v)
  }
}