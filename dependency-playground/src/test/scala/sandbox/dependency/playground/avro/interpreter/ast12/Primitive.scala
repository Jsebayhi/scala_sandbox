package sandbox.dependency.playground.avro.interpreter.ast12

import primitive._

trait Primitive[T] {
  def v: T
}

case class ListStringPrim(v: List[StringPrim]) extends Primitive[List[StringPrim]]
  with StringPrimContainsable {

  override def doesContain(candidate: StringPrim): Boolean = {
    v.contains(candidate)
  }
}

case class StringPrim(v: String) extends Primitive[String]
  with StringPrimContainsable
  with StringPrimEqualsable
  with StringPrimStartsWithable
  with IntPrimEqualsable {

  override def doesContain(candidate: StringPrim): Boolean = {
    v.contains(candidate.v)
  }

  override def isEqual(candidate: StringPrim): Boolean = {
    v == candidate.v
  }

  override def doesStartsWith(candidate: StringPrim): Boolean = {
    v.startsWith(candidate.v)
  }

  override def isEqual(candidate: IntPrim): Boolean = {
    v == candidate.v.toString
  }
}

case class IntPrim(v: Int) extends Primitive[Int]
  with IntPrimEqualsable
  with StringPrimEqualsable {

  override def isEqual(candidate: IntPrim): Boolean = {
    v == candidate.v
  }

  override def isEqual(candidate: StringPrim): Boolean = {
    v.toString == candidate.v
  }
}