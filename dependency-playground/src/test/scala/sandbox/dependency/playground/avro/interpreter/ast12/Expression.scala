package sandbox.dependency.playground.avro.interpreter.ast12

import org.apache.avro.generic.GenericRecord

import scala.util.Try

trait Expression {
  def eval(container: GenericRecord): Try[Boolean]
}