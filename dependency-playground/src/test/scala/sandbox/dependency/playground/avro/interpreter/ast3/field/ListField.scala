package sandbox.dependency.playground.avro.interpreter.ast3.field

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import com.carrefour.phenix.tradeitem.mapping.playground.ast3.behavior.Containsable

import scala.util.{ Failure, Try }

case class ListField[T](fieldName: String) extends Containsable[T] {

  private def cast(a: AnyRef): List[T] = Try(a.asInstanceOf[List[T]])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a String", e)) }
    .get

  override def doesContain(container: GenericContainer[AnyRef], candidate: T): Boolean = {
    cast(container.get(fieldName)
      .getOrElse(throw new RuntimeException(s"incompatible schema, the field $fieldName is unknown to this schema (${container.schema()}"))
    ).contains(candidate)
  }
}
