package sandbox.dependency.playground.avro.interpreter.ast6

import com.carrefour.phenix.tradeitem.mapping.playground.ast6.behavior.Equalsable
import org.apache.avro.generic.GenericRecord

case class Equals[T](fieldAccessor: Equalsable[T], v: T) extends Expression {
  override def eval(container: GenericRecord): Boolean = {
    fieldAccessor.isEqual(container, v)
  }
}