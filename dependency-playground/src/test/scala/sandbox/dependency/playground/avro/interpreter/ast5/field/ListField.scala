package sandbox.dependency.playground.avro.interpreter.ast5.field

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.behavior.Containsable
import com.carrefour.phenix.tradeitem.mapping.playground.ast5.accessor.Accessor

import scala.util.{ Failure, Try }

case class ListField[T](accessor: Accessor) extends Containsable[T] {

  private def cast(a: AnyRef): List[T] = Try(a.asInstanceOf[List[T]])
    .recoverWith { case e => Failure(new RuntimeException("given anyref is not a List", e)) }
    .get

  override def doesContain(container: GenericContainer[AnyRef], candidate: T): Boolean = {
    cast(accessor.access(container)).contains(candidate)
  }
}
