package sandbox.scalapl.interpreter.playground.ast10

import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord

import scala.util.{ Failure, Try }

trait Promised[T]

case class PromisedList[T](path: Seq[String]) extends Promised[List[T]]
  with Containsable[T] {

  private def cast(o: ObjectWithType): Try[List[T]] = {
    o.typ match {
      case Schema.Type.ARRAY => Try(o.obj.asInstanceOf[List[T]]) //TODO: handle the mapping of the type in the array
        .recoverWith { case e: Exception => Failure(TypeMismatchException(o.typ.getName, "List[T]", e)) }
      case _ => Failure(TypeMismatchException(o.typ.getName, "List[T]"))
    }
  }

  override def doesContain(container: GenericRecord, candidate: T): Boolean = {
    GenericRecordFieldAccessor.accessNestedField(path, container)
      .flatMap(lst => cast(lst))
      .map(_.contains(candidate))
      .get
  }
}

case class PromisedString(path: Seq[String]) extends Promised[String]
  with Containsable[String]
  with Equalsable[String]
  with StartsWithable[String] {

  private def cast(o: ObjectWithType): Try[String] = {
    o.typ match {
      case Schema.Type.STRING => Try(o.obj.asInstanceOf[String])
        .recoverWith { case e: Exception => Failure(TypeMismatchException(o.typ.getName, "String", e)) }
      case _ => Failure(TypeMismatchException(o.typ.getName, "String"))
    }
  }

  override def doesContain(container: GenericRecord, candidate: String): Boolean = {
    GenericRecordFieldAccessor.accessNestedField(path, container)
      .flatMap(lst => cast(lst))
      .map(_.contains(candidate))
      .get
  }

  override def isEqual(container: GenericRecord, candidate: String): Boolean = {
    GenericRecordFieldAccessor.accessNestedField(path, container)
      .flatMap(lst => cast(lst))
      .map(_ == candidate)
      .get
  }

  override def doesStartsWith(container: GenericRecord, candidate: String): Boolean = {
    GenericRecordFieldAccessor.accessNestedField(path, container)
      .flatMap(lst => cast(lst))
      .map(_.startsWith(candidate))
      .get
  }
}

case class PromisedInt(path: Seq[String]) extends Promised[Int]
  with Equalsable[Int] {

  private def cast(o: ObjectWithType): Try[Int] = {
    o.typ match {
      case Schema.Type.INT => Try(o.obj.asInstanceOf[Int])
        .recoverWith { case e: Exception => Failure(TypeMismatchException(o.typ.getName, "Int", e)) }
      case _ => Failure(TypeMismatchException(o.typ.getName, "Int"))
    }
  }

  override def isEqual(container: GenericRecord, candidate: Int): Boolean = {
    GenericRecordFieldAccessor.accessNestedField(path, container)
      .flatMap(lst => cast(lst))
      .map(_ == candidate)
      .get
  }
}

abstract class PrimitiveException(msg: String, cause: Exception = null) extends RuntimeException(msg, cause)

case class TypeMismatchException(thing: Object, expectedType: String, cause: Exception = null)
  extends PrimitiveException(s"type mismatch, $thing is not a $expectedType.", cause)