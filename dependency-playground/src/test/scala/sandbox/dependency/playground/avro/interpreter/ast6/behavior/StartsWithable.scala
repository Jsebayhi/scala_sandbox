package sandbox.dependency.playground.avro.interpreter.ast6.behavior

import org.apache.avro.generic.GenericRecord

trait StartsWithable[T] {
  def doesStartsWith(container: GenericRecord, fieldContent: T): Boolean
}