package sandbox.dependency.playground.avro.interpreter.ast4

import com.carrefour.phenix.tradeitem.mapping.playground.AnyRefContainer.AnyRefContainer

trait Expression {
  def eval(container: AnyRefContainer): Boolean
}