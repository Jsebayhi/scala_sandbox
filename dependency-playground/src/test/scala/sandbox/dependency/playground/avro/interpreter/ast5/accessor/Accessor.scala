package sandbox.dependency.playground.avro.interpreter.ast5.accessor

import com.carrefour.phenix.tradeitem.mapping.playground.GenericContainer

trait Accessor {
  def access(container: GenericContainer[AnyRef]): AnyRef
}