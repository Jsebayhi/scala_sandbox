package sandbox.dependency.playground

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class PlaygroundSpec extends AnyFlatSpec with Matchers {

  import org.json4s.jackson.JsonMethods.parse
  import org.json4s.DefaultFormats
  implicit val defaultFormat = DefaultFormats

  "test" should "be succesful" in {
    val json =
      """{
        |"e": 10,
        |"t": "A"
        |}
        |""".stripMargin
    val in = AClass(e = 10, Enum.A)

    parse(json).extract[AClass] shouldBe in
  }
  it should "not be succesful" in {
    val json =
      """{
        |"t": "C"
        |}""".stripMargin

    parse(json).extract[AClass]
  }

}

case class AClass(e: Int, t: Enum.Enum)

object Enum extends Enumeration {
  type Enum = Value
  val A, B = Value
}

