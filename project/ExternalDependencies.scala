import sbt._

object ExternalDependencies {

  lazy val ScalatraDependencies = {
    val ScalatraVersion = "2.7.0"
    Seq(
      "org.scalatra" %% "scalatra" % ScalatraVersion,
      "org.scalatra" %% "scalatra-json" % ScalatraVersion,
      "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test"
    )
  }

  lazy val ApiMockWithScalatraDependencies = ScalatraDependencies

  lazy val scalaTestDependencies = Seq(
    "org.scalatest" %% "scalatest" % "3.2.0" % "test",
    "org.scalatest" %% "scalatest-flatspec" % "3.2.0" % "test",
    "org.scalatest" %% "scalatest-matchers-core" % "3.2.0" % "test",
    "org.scalamock" %% "scalamock" % "4.4.0" % "test"
  )

  lazy val jodaTimeDependencies = Seq(
    "joda-time" % "joda-time" % "2.3",
    "org.joda" % "joda-convert" % "1.3.1"
  )
}
