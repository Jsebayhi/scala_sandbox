import sbt.Keys.libraryDependencies
import scalariform.formatter.preferences._
import ExternalDependencies._

lazy val commonSettings = Seq(
  organization := "sebayhi.jeremy",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.13.0",
  IntegrationTest / parallelExecution  := false,
  scalariformPreferences := scalariformPreferences.value
    .setPreference(AlignSingleLineCaseStatements, true)
    .setPreference(DoubleIndentConstructorArguments, true)
    .setPreference(DanglingCloseParenthesis, Preserve),
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding", "UTF-8",
    "-feature",
    "-explaintypes",
    "-language:existentials",
    "-Xfatal-warnings"
  )
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    name := "scala-sandbox"
  ).aggregate(
  benchmarkDemo,
  csvParsers,
  //dependencyPlayground,
  scalaProgrammingLanguage,
  templateModule
)

/*
*** MODULES
 */

lazy val templateModule = scalaTestWithJmhBenchmarkProject("template-module")

/*
lazy val dependencyPlayground = scalaTestWithJmhBenchmarkProject("dependency-playground")
  .settings(
    libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.9",
    libraryDependencies += "org.json4s" %% "json4s-native" % "3.6.9",
    libraryDependencies ++= ApiMockWithScalatraDependencies
  )

 */

lazy val scalaProgrammingLanguage = scalaTestWithJmhBenchmarkProject("scala-pl")

lazy val csvParsers = scalaTestWithJmhBenchmarkProject("csv-parsers")
  .settings(
    //scala-scv
    libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.6",
    //kantan for scala 2.12+
    libraryDependencies += "com.nrinaudo" %% "kantan.csv" % "0.6.1",
    //kantan Java 8 date and time instances.
    libraryDependencies += "com.nrinaudo" %% "kantan.csv-java8" % "0.6.1",
    //kanatan case class handling with shapeless.
    libraryDependencies += "com.nrinaudo" %% "kantan.csv-generic" % "0.6.1",
    //kantan cats type class instances.
    //libraryDependencies += "com.nrinaudo" %% "kantan.csv-cats" % "0.6.1"
    //univocity
    libraryDependencies += "com.univocity" %  "univocity-parsers" % "2.5.9",
    libraryDependencies += "commons-io"    %  "commons-io"        % "2.6"

  )

/*
*** BENCHMARKS
 */

lazy val benchmarkDemo = jmhBenchmarkProject("benchmark-demo")

/*
*** PROJECT BUILDERS
 */

def scalaTestWithJmhBenchmarkProject(directory: String) = {
  enableJmhBenchmarkPlugin(scalaTestProject(directory))
}

def jmhBenchmarkProject(directory: String) = {
  enableJmhBenchmarkPlugin(projectBuilder(directory))
}

def scalaTestProject(directory: String) = {
  projectBuilder(directory)
    .settings(libraryDependencies ++= scalaTestDependencies)
}

def enableJmhBenchmarkPlugin(project: Project): Project = {
  project.enablePlugins(JmhPlugin)
}

def projectBuilder(directory: String, settings: SettingsDefinition = commonSettings): Project = {
  Project(directory, file(directory)).settings(settings)
}
