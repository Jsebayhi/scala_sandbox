object Test {

   def main(args: Array[String]) {
        val input = "application/vdn.ta.to.ti-v0+x-binary-avro, application/json"


        val HeaderName = "Accept"
        val SchemaNamespace = "sale.v2"
        val SchemaName = "TicketHeader"
        val SchemaRecord = SchemaNamespace + "." + SchemaName
        val DefaultContentType = "json"
        val DefaultSchemaVersion = 0
        val validSchemaVersions = List("0", "1")
        val validContentType = List("json", "x-binary-avro")
        val DefaultValue = s"application/vdn.$SchemaRecord-v${DefaultSchemaVersion.toString}+$DefaultContentType"

        val Pattern = "application/(vdn\\..*\\-v\\d+\\+)?(json|x\\-binary\\-avro)"
        val PatternExtractor = """application/vdn\.(.+)-v(\d+)\+(.+)""".r //TODO: add check for json and avro

          val noVersionSpecified = """application/(json|x\-binary\-avro)"""
          val noAcceptSpecified = """\*/\*"""
          val browserTipicalContentRequest = """text/html"""


            val content = s"error, application/x-binary-avro, application/json, application/vdn.${SchemaRecord}-v0+json"

          val contentList = content.split(',').map(x => x.trim)
          contentList.map(x => println(x))
          val valid = contentList.filter {
            case PatternExtractor(record, version, contentType) if (record == SchemaRecord
              && validSchemaVersions.contains(version)
              && validContentType.contains(contentType)) => true
            case c if c.matches(noVersionSpecified) => true
            case c if c.matches(noAcceptSpecified) => true
            case c if c.matches(browserTipicalContentRequest) => true
            case _ => false
          }
          valid.map(x => println(x))
          if (!valid.isEmpty){
            val res = valid.head.mkString("") match {
                case PatternExtractor(record, version, contentType) if (record == SchemaRecord
                  && validSchemaVersions.contains(version)
                  && validContentType.contains(contentType)) => true
                case c if c.matches(noVersionSpecified) => true
                case c if c.matches(noAcceptSpecified) => true
                case c if c.matches(browserTipicalContentRequest) => true
                case _ => false
            }
          println(res)
          }
        //extract(input)
   }

   def extract(input: String) = {
        //val reg = """(.*)\.(.*)""".r
        val reg = """application/vdn\.(.+)-v(\d+)\+(.*)""".r
        input match {
            case reg(extract, extract2, extract3) => println(extract + " " + extract2 + " " + extract3)
            case _ => println("")
        }

   }
}

