package sandbox.template

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import sandbox.template.Template._

class TemplateSpec extends AnyFlatSpec with Matchers {
  "test" should "be succesful" in {
    identityFunction("parameter") shouldBe "parameter"
  }
}

