object HelloWorld {
    def test(content: String) = {
        val SchemaNamespace = "sale.v2"
        val SchemaName = "TicketHeader"
        val SchemaRecord = SchemaNamespace + "." + SchemaName
        val DefaultSchemaVersion = 0
        val PatternExtractor = """application/vdn\.(.*)\.(.*)-v(\d+)\+.*""".r
        
        val noVersionSpecified = """application/(json|x\-binary\-avro)"""
        val noAcceptSpecified = """\*/\*"""
        val browserTipicalContentRequest = """text/html"""
        val contentList = content.split(',')
        var valid = contentList.filter {
            case PatternExtractor(nameSpace, name, version) => true
            case c if c.matches(noVersionSpecified) => true
            case c if c.matches(noAcceptSpecified) => true
            case c if c.matches(browserTipicalContentRequest) => true
            case _ => false
        }
        if (!valid.isEmpty) {
            valid.head.mkString("") match {
                case PatternExtractor(nameSpace, name, version) => Some(SchemaSimplified(nameSpace, name, version.toInt))
                case c if c.matches(noVersionSpecified) => Some(SchemaSimplified(SchemaNamespace, SchemaName, DefaultSchemaVersion))
                case c if c.matches(noAcceptSpecified) => Some(SchemaSimplified(SchemaNamespace, SchemaName, DefaultSchemaVersion))
                case c if c.matches(browserTipicalContentRequest) => Some(SchemaSimplified(SchemaNamespace, SchemaName, DefaultSchemaVersion))
                case _ => None: Option[SchemaSimplified]
            }
        } else {
            None: Option[SchemaSimplified]
        }
    }

   def main(args: Array[String]) {
       val res = test("application/json, application/x-binary-avro")
       res.map(e => println(e))
   }
   







    case class SchemaSimplified(Namespace: String, Name: String, Version: Int) {
        def record = {
            Namespace + "." + Name
        }
    }

    object SchemaSimplified {
        def apply(Record: String, version: Int): SchemaSimplified = {
            val splitted = Record.split('.')
            val Name = { if (splitted.isEmpty) { "" } else { splitted.last } }
            val Namespace = { if (splitted.isEmpty) { "" } else { splitted.dropRight(1).mkString(".") } }
            new SchemaSimplified(Namespace, Name, version)
        }
    }
}

