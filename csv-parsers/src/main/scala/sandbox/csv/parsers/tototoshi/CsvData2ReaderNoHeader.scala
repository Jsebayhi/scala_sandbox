package sandbox.csv.parsers.tototoshi

import java.io.File

import com.github.tototoshi.csv.{ CSVReader, DefaultCSVFormat }
import models._
import sandbox.csv.parsers._

import scala.util.Try

object CsvData2ReaderNoHeader {
  implicit object MyFormat extends DefaultCSVFormat {
    override val delimiter = ','
  }

  def read(file: File): Iterator[Either[ParsingError, Data2]] = {
    val csv = CSVReader.open(file)
    csv.iterator
      .map(m => readLine(m))
  }

  def readLine(m: Seq[String]): Either[ParsingError, Data2] = {
    val nb: Either[String, Int] = {
      m.headOption
        .toRight("missing 'number' column for this row")
        .flatMap {
          str =>
            Try(str.toInt)
              .toEither
              .left.map(_ => s"bad data in the 'number' column, not an Int: $str")
        }
    }
    val str: Either[String, String] = {
      if (m.size >= 2) {
        Right(m(1))
      } else {
        Left("missing 'string' column for this row")
      }
    }

    if (nb.isRight && str.isRight) {
      Right(Data2(
        nb = nb.getOrElse(throw new AssertionError("wtf! should never have ended up in here")),
        str = str.getOrElse(throw new AssertionError("wtf! should never have ended up in here"))
      ))
    } else {
      Left(PoorBadRowParsingError.from(Array(nb, str)))
    }
  }
}
