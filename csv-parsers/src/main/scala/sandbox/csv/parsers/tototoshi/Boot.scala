package sandbox.csv.parsers.tototoshi

import java.io.File

import com.github.tototoshi.csv.CSVWriter

//https://github.com/tototoshi/scala-csv
object Boot extends App {

  def run = {
    val (res, errors) = CsvData2ReaderNoHeader.read(new File("/tmp/10mLines.csv"))
      .partition(_.isRight)

    val writer = CSVWriter.open("/tmp/deleteme", true)

    errors.map(_.left.getOrElse(throw new AssertionError("wtf! should never have ended up in here")))
      .foreach(e => println(e.format))

    res
      .map(_.getOrElse(throw new AssertionError("wtf! should never have ended up in here")))
      .map(d => Seq(d.nb, d.str))
      .foreach { d =>
        println(s"writing $d")
        writer.writeRow(d)
      }

    writer.flush()
    writer.close()
  }

  run
}
