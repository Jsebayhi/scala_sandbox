package sandbox.csv.parsers.tototoshi

import java.io.File

import com.github.tototoshi.csv.{ CSVReader, DefaultCSVFormat }
import models._
import sandbox.csv.parsers._

import scala.util.Try

object CsvData1ReaderWithHeader {
  implicit object MyFormat extends DefaultCSVFormat {
    override val delimiter = '|'
  }

  def read(file: File): Iterator[Either[ParsingError, Data1]] = {
    val csv = CSVReader.open(file)
    csv.toStreamWithHeaders.iterator
      .map(m => readLine(m))
  }

  def readLine(m: Map[String, String]): Either[ParsingError, Data1] = {
    val prenom: Either[String, String] = {
      m.get("prenom").toRight("missing prenom column for this row")
    }
    val nom: Either[String, String] = {
      m.get("nom").toRight("missing nom column for this row")
    }
    val surnom: Either[String, String] = {
      m.get("surnom").toRight("missing surnom column for this row")
    }
    val age: Either[String, Int] = m.get("age")
      .toRight("missing age column for this row")
      .flatMap {
        str =>
          Try(str.toInt)
            .toEither
            .left.map(_ => s"bad data in the 'age' column, not an Int: $str")
      }

    if (prenom.isRight && nom.isRight && surnom.isRight && age.isRight) {
      Right(Data1(
        prenom = prenom.getOrElse(throw new AssertionError("wtf! should never have ended up in here")),
        nom = nom.getOrElse(throw new AssertionError("wtf! should never have ended up in here")),
        surnom = surnom.getOrElse(throw new AssertionError("wtf! should never have ended up in here")),
        age = age.getOrElse(throw new AssertionError("wtf! should never have ended up in here"))
      ))
    } else {
      Left(PoorBadRowParsingError.from(Array(prenom, nom, surnom, age)))
    }
  }
}
