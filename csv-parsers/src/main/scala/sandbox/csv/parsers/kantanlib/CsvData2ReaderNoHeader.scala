package sandbox.csv.parsers.kantanlib

import java.io.File

import models._
import sandbox.csv.parsers._

import kantan.csv._
import kantan.csv.ops._

object CsvData2ReaderNoHeader {

  def read(file: File): Iterator[Either[ParsingError, Data2]] = {
    val config: CsvConfiguration = rfc.withoutHeader.withCellSeparator('|')

    implicit val decoder: HeaderDecoder[Data2] = {
      HeaderDecoder.decoder("nb", "str")(Data2.apply _)
    }

    file.asCsvReader[Data2](config)
      .iterator
      .map(_.left.map(WrappedKantanError.apply))
  }
}
