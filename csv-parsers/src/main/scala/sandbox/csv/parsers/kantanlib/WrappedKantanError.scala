package sandbox.csv.parsers.kantanlib

import sandbox.csv.parsers.ParsingError

case class WrappedKantanError(err: kantan.codecs.error.Error) extends ParsingError {
  override def format: String = err.toString()
}
