package sandbox.csv.parsers.kantanlib

import java.io.File
import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._
import models._
import sandbox.csv.parsers._

object CsvData1ReaderWithHeader {

  def read(file: File): Iterator[Either[ParsingError, Data1]] = {

    val config: CsvConfiguration = rfc.withHeader.withCellSeparator('|')

    file.asCsvReader[Data1](config)
      .iterator
      .map(_.left.map(WrappedKantanError.apply))
  }
}
