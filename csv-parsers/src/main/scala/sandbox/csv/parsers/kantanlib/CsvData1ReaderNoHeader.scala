package sandbox.csv.parsers.kantanlib

import java.io.File

import kantan.csv._
import kantan.csv.ops._
import models._
import sandbox.csv.parsers._

object CsvData1ReaderNoHeader {

  def read(file: File): Iterator[Either[ParsingError, Data1]] = {
    val config: CsvConfiguration = rfc.withoutHeader.withCellSeparator('|')

    implicit val decoder: HeaderDecoder[Data1] = {
      HeaderDecoder.decoder("prenom", "nom", "surnom", "age")(Data1.apply _)
    }

    file.asCsvReader[Data1](config)
      .iterator
      .map(_.left.map(WrappedKantanError.apply))
  }
}
