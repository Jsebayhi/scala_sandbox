package sandbox.csv.parsers.kantanlib

import java.io.File

import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._
import models.Data2

//http://nrinaudo.github.io/kantan.csv/
object Boot extends App {

  def run = {

    val out = java.io.File.createTempFile("deleteme", ".csv")
    val writer = out.asCsvWriter[Data2](rfc.withHeader("prenom", "nom", "surnom", "age"))

    CsvData2ReaderNoHeader.read(new File("/tmp/10mLines.csv"))
      .foreach {
        case Right(datum) => writer.write(datum)
        case Left(err)    => println(err.format)
      }

    writer.close()
  }

  run
}
