package sandbox.csv.parsers.univocity

import java.io.{ File, InputStream }

import com.univocity.parsers.csv.{ CsvParser, CsvParserSettings }
import models._
import sandbox.csv.parsers._

import scala.util.Try

object CsvData1ReaderNoHeader {

  val settings = new CsvParserSettings()

  def read(input: InputStream): Iterator[Either[ParsingError, Data1]] = {
    val reader = new CsvParser(settings)

    reader.parseAsStream(input)
      .map(m => readLine(m))
      .iterator
  }

  def readLine(m: Array[String]): Either[ParsingError, Data1] = {
    val prenom: Either[String, String] = {
      m.headOption.toRight("missing prenom column for this row")
    }
    val nom: Either[String, String] = {
      if (m.length >= 2) {
        Right(m(1))
      } else {
        Left("missing nom column for this row")
      }

    }
    val surnom: Either[String, String] = {
      if (m.length >= 3) {
        Right(m(2))
      } else {
        Left("missing surnom column for this row")
      }
    }
    val age: Either[String, Int] = {
      if (m.length >= 4) {
        Right(m(3))
          .flatMap {
            str =>
              Try(str.toInt)
                .toEither
                .left.map(_ => s"bad data in the 'age' column, not an Int: $str")
          }
      } else {
        Left("missing age column for this row")
      }
    }

    if (prenom.isRight && nom.isRight && surnom.isRight && age.isRight) {
      Right(Data1(
        prenom = prenom.getOrElse(throw new AssertionError("wtf! should never have ended up in here")),
        nom = nom.getOrElse(throw new AssertionError("wtf! should never have ended up in here")),
        surnom = surnom.getOrElse(throw new AssertionError("wtf! should never have ended up in here")),
        age = age.getOrElse(throw new AssertionError("wtf! should never have ended up in here"))
      ))
    } else {
      Left(PoorBadRowParsingError.from(Array(prenom, nom, surnom, age)))
    }
  }
}
