package sandbox.csv.parsers

import java.io._
import org.apache.commons.io.input.BOMInputStream
import com.univocity.parsers.csv.CsvParser

package object univocity {

  import scala.language.implicitConversions
  implicit class RichCsvParser(parser: CsvParser) {

    def parseAsStream(f: String): LazyList[Array[String]] = {
      parseAsStream(new FileInputStream(f))
    }

    def parseAsStream(input: InputStream): LazyList[Array[String]] = {

      // XXX: resources will be automatically closed at the end of input,
      //      no need to manually close the parser later.
      parser.beginParsing(new BOMInputStream(input))

      def loop: LazyList[Array[String]] = {
        val row = parser.parseNext()
        if (row == null) {
          LazyList.empty
        } else {
          replaceNullsMutable(row) #:: loop
        }
      }

      loop
    }

    /*
   * XXX: Mutable for performance.
   */
    private def replaceNullsMutable(row: Array[String]): Array[String] = {
      row.indices.foreach { i =>
        val v = row(i)
        if (v == null) row(i) = ""
      }
      row
    }
  }

}
