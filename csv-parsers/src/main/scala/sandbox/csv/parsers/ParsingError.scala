package sandbox.csv.parsers

trait ParsingError {
  def format: String
}
/*
object BadRowParsingError {
  def apply(line: Long, reason: String): BadRowParsingError = {
    BadRowParsingError(line, Array(reason))
  }
  def apply(line: Long, reasons: Iterable[Either[String, _]]): BadRowParsingError = {
    BadRowParsingError(line, reasons.filter(_.isLeft).map(_.left.get))
  }
}
case class BadRowParsingError(lineNb: Long,
                              reasons: Iterable[String]) extends ParsingError {
  override def format: String = {
    s"for row at line $lineNb, the following error were found:" + reasons
      .mkString("\n", "\n-", "")
  }
}
 */

object PoorBadRowParsingError {
  def from(reasons: Iterable[Either[String, _]]): PoorBadRowParsingError = {
    PoorBadRowParsingError(reasons.filter(_.isLeft).map(_.left.getOrElse(throw new AssertionError("wtf! should never have ended up in here"))))
  }
}
case class PoorBadRowParsingError(reasons: Iterable[String]) extends ParsingError {
  require(reasons.nonEmpty, "creating a row parsing error without any error...")
  override def format: String = {
    s"for row at an unknown line, the following error were found:" + reasons
      .mkString("\n", "\n-", "")
  }
}