package sandbox.csv.parsers.tototoshi

import java.io.File

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import com.github.tototoshi.csv._
import resources._
import models._
import sandbox.csv.parsers.ParsingError

class ParserSpec extends AnyFlatSpec with Matchers {
  behavior of "tototoshi lib"

  it should "work with headers" in {
    val file: File = new File(this.getClass.getClassLoader.getResource(CsvHeader.fileName).getPath)
    val res: Iterator[Either[ParsingError, Data1]] = CsvData1ReaderWithHeader.read(file)
    res.toList should contain theSameElementsInOrderAs (CsvHeader.content.map(e => Right(e)))
  }

  it should "work with file which do not have a headers" in {
    val file: File = new File(this.getClass.getClassLoader.getResource(CsvNoHeader.fileName).getPath)
    val res: Iterator[Either[ParsingError, Data1]] = CsvData1ReaderNoHeader.read(file)
    res.toList should contain theSameElementsInOrderAs (CsvNoHeader.content.map(e => Right(e)))
  }

  ignore should "work with file which have variable line sizes" in {

  }

  ignore should "log non fatal error found in lines but still return a partial result" in {

  }

  ignore should "work with files which have more information than necessary" in {

  }

  ignore should "work with big files" in {

  }

  ignore should "allow clear and concise handling of error per line" in {

  }

  ignore should "allow retrocompatibility on files" in {

  }

}
