import models._

package object resources {

  case class CsvResources[T](
      fileName: String,
      headers: Option[Seq[String]] = None,
      content: Seq[T])

  case class BigCsvResources[T](
      fileName: String,
      contentSize: Long,
      content: Seq[T])

  case class Csv1Data(prenom: String, nom: String, surnom: String, age: Int)

  val CsvHeader: CsvResources[Data1] = CsvResources[Data1](
    fileName = "headers.csv",
    headers = Some(Seq("prénom", "nom", "surnom", "age")),
    content = Seq(
      Data1(prenom = "titi", nom = "l'oiseau", surnom = "cours toujours!", age = 3),
      Data1(prenom = "gros minet", nom = "le chat", surnom = "je l'aurai un jour!", age = 7)
    )
  )

  val CsvNoHeader: CsvResources[Data1] = CsvResources[Data1](
    fileName = "no-headers.csv",
    headers = None,
    content = Seq(
      Data1(prenom = "titi", nom = "l'oiseau", surnom = "cours toujours!", age = 3),
      Data1(prenom = "gros minet", nom = "le chat", surnom = "je l'aurai un jour!", age = 7)
    )
  )

  /*
  val CsvNoHeader: CsvResources[(String, String, String)] = CsvResources[(String, String, String)](
    fileName = "no-headers.csv",
    content = Seq(
      ("titi","l'oiseau","cours toujours!"),
      ("gros minet","le chat","je l'aurai un jour!")
    )
  )
  */

  val Csv100mLines: BigCsvResources[(Int, String)] = BigCsvResources[(Int, String)](
    fileName = "100mLines.csv",
    contentSize = 100000000,
    content = { for (i <- 1 to 1000) yield (i, i.toString) }
  )

}
