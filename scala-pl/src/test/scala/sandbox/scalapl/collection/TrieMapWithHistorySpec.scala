package sandbox.scalapl.collection

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class TrieMapWithHistorySpec extends AnyFlatSpec with Matchers {
  it should "correctly add to queue and TrieMap" in {
    val HistorySizeMax = 3
    val trieMapWithHistory = new TrieMapWithHistory[String, String](HistorySizeMax)

    trieMapWithHistory.getOrElse("index1", "index1_content")
    trieMapWithHistory.getOrElse("index2", "index2_content")
    trieMapWithHistory.getOrElse("index3", "index3_content")

    trieMapWithHistory.container.size shouldBe HistorySizeMax
    trieMapWithHistory.container shouldBe scala.collection.concurrent.TrieMap(
      "index1" -> "index1_content",
      "index2" -> "index2_content",
      "index3" -> "index3_content")
    trieMapWithHistory.indexHistory.size shouldBe HistorySizeMax
    trieMapWithHistory.indexHistory shouldBe scala.collection.mutable.Queue(
      "index1",
      "index2",
      "index3")
  }
  it should "correctly keep queue and TrieMap to the specified size" in {
    val HistorySizeMax = 3
    val trieMapWithHistory = new TrieMapWithHistory[String, String](HistorySizeMax)

    trieMapWithHistory.getOrElse("index1", "index1_content")
    trieMapWithHistory.getOrElse("index2", "index2_content")
    trieMapWithHistory.getOrElse("index3", "index3_content")
    trieMapWithHistory.getOrElse("index4", "index4_content")

    trieMapWithHistory.container.size shouldBe HistorySizeMax
    trieMapWithHistory.container shouldBe scala.collection.concurrent.TrieMap(
      "index2" -> "index2_content",
      "index3" -> "index3_content",
      "index4" -> "index4_content")
    trieMapWithHistory.indexHistory.size shouldBe HistorySizeMax
    trieMapWithHistory.indexHistory shouldBe scala.collection.mutable.Queue(
      "index2",
      "index3",
      "index4")
  }
  it should "correctly not recreate index" in {
    val HistorySizeMax = 3
    val trieMapWithHistory = new TrieMapWithHistory[String, String](HistorySizeMax)

    trieMapWithHistory.getOrElse("index1", "index1_content") shouldBe "index1_content"
    trieMapWithHistory.getOrElse("index2", "index2_content") shouldBe "index2_content"
    trieMapWithHistory.getOrElse("index3", "index3_content") shouldBe "index3_content"
    trieMapWithHistory.getOrElse("index4", "index4_content") shouldBe "index4_content"
    trieMapWithHistory.getOrElse("index2", "index2_content") shouldBe "index2_content"

    trieMapWithHistory.container.size shouldBe HistorySizeMax
    trieMapWithHistory.container shouldBe scala.collection.concurrent.TrieMap(
      "index2" -> "index2_content",
      "index3" -> "index3_content",
      "index4" -> "index4_content")
    trieMapWithHistory.indexHistory.size shouldBe HistorySizeMax
    trieMapWithHistory.indexHistory shouldBe scala.collection.mutable.Queue(
      "index2",
      "index3",
      "index4")
  }
}

