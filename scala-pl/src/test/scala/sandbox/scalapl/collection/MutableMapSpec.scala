package sandbox.scalapl.collection

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class MutableMapSpec extends AnyFlatSpec with Matchers {

  it should "add an element to the map" in {
    val m = scala.collection.mutable.Map.empty[Int, String]
    m.getOrElse(1, "toto") shouldBe "toto"
    m += (2 -> "titi")
    m.get(2) shouldBe Some("titi")
  }
}
