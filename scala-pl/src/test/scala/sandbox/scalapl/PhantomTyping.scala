package sandbox.scalapl

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class PhantomTyping extends AnyFlatSpec with Matchers {

  import Tag._
  sealed trait NameTag
  sealed trait SurnameTag
  type Name = Tagged[String, NameTag]
  type Surname = Tagged[String, SurnameTag]

  "tag" should "allow to tag a string" in {
    val name: Name = Tag[String, NameTag]("valid")
    val surname: Surname = Tag[String, SurnameTag]("valid")
  }
  it should "fail since the type does not match" in {
    //won't work
    //val supposedToBeAName: Name = Tag[String, SurnameTag]("valid")
  }

  trait Table
  "string" should "taggable with specific type" in {
    val someTable: String with Table = "someTable".asInstanceOf[String with Table]
  }
  it should "be usable as a simple String even though tagged" in {
    val notNecessarilyTable: String = "not a table".asInstanceOf[String with Table]
  }
  it should "not compile if we expected a tagged String" in {
    //do not compile - val supposedlyATable: String with Table = "not a table"
  }

  sealed trait DoorState
  sealed trait Open extends DoorState
  sealed trait Closed extends DoorState

  case class Door[State <: DoorState]() {
    def open(implicit ev: State =:= Closed) = Door[Open]()
    def close(implicit ev: State =:= Open) = Door[Closed]()
  }

  "door example" should "fail at compile time when trying to close a closed door" in {
    val door = Door[Closed]
    //door.close
  }
  it should "not fail at compile time when trying to open a closed door" in {
    val door = Door[Closed]
    door.open
  }
  it should "not fail at compile time when trying to close an opened door" in {
    val door = Door[Open]
    door.close
  }
  it should "fail at compile time when trying to open an opened door" in {
    val door = Door[Open]
    //door.open
  }
}