package sandbox.scalapl

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class FunctionnalSpec extends AnyFlatSpec with Matchers {

  "andThen" should "work" in {
    def iToA(i: Int): String = i.toString
    def isEmpty(str: String): Boolean = str.isEmpty

    (iToA _).andThen(isEmpty)(9) shouldBe false
  }

}
