package sandbox.scalapl

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MatchSpec extends AnyFlatSpec with Matchers {

  "match" should "be made more readable using @ like 'case (left@(v1,_), right@(v2,_)) => if(v1>v2) left else right'" in {
    val ExpectedRes = (3, 2)
    val Noise = (2, 3)
    val res = (ExpectedRes, Noise) match {
      case (left @ (v1, _), right @ (v2, _)) => if (v1 > v2) left else right
    }
    res shouldBe ExpectedRes
  }
}
