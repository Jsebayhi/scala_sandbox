package sandbox.scalapl

object PrettyFixtures extends ImplicitOptions

trait ImplicitOptions {
  import scala.language.implicitConversions
  implicit def typeToOption[T](thing: T): Option[T] = {
    Some(thing)
  }
}