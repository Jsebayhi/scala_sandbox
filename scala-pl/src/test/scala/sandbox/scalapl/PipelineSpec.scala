package sandbox.scalapl

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

object obj {}

/*
class PipelineSpec extends AnyFlatSpec with Matchers {

  it should "do" in {
    import PipeResult.toPipeResult
    //import SimplePipeResult.toSimplePipeResult
    val e: List[String] = List(1, 2, 3, 4, 5, 6, 7, 8).partition(x => x % 2 == 0).actOn[List[String]]{
      res =>
        val t = res.actOnRight[List[Int]]{ r =>
          r.map(x => x * 2)
        }
        res.actOnLeft[Unit]{ l => println(l.map(_.toString)) }
        t.map(_.toString)
    }

  }
}

case class PipeResult[L, R](private val left: L, private val right: R) {
  def actOn[T](f: (this.type) => T): T = {
    f(this)
  }
  def actOnLeft[D, G](f: L => PipeResult[D, G]): Unit = {
    f(left)
  }
  def actOnLeft[T](f: L => T): T = {
    f(left)
  }
  def actOnRight[D, G](f: R => PipeResult[D, G]): Unit = {
    f(right)
  }
  def actOnRight[T](f: R => T): T = {
    f(right)
  }
}
object PipeResult {
  def apply[L, R](tuple: (L, R)): PipeResult[L, R] = new PipeResult(tuple._1, tuple._2)
  implicit def toPipeResult[L, R](tuple: (L, R)): PipeResult[L, R] = this.apply(tuple)
}

case class SimplePipeResult[T](r: T) {
  def actOn[U, V](f: (T) => U): Unit = {
    f(r)
  }
}
object SimplePipeResult {
  implicit def apply[T](x: T): SimplePipeResult[T] = new SimplePipeResult[T](x)
  //implicit def toSimplePipeResult[T](x: T): SimplePipeResult[T] = new SimplePipeResult[T](x)
}
*/ 