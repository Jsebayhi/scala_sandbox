package sandbox.scalapl.inheritance

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class ValOverrideSpec extends AnyFlatSpec with Matchers {

  it should "work" in {
    val t = new ImplementedClass()
    t.finalConf shouldBe "tototata"
  }

  it should "fail" in {
    val t = new ImplementedClassFail()
    t.finalConf shouldBe "totonull"
  }

  it should "fail for abstract class as well" in {
    val t = new ImplementedAbstractClassFail()
    t.finalConf shouldBe "totonull"
  }
}

trait SuperClass {

  val conf: String

  def finalConf = s"toto$conf"
}

class ImplementedClass extends SuperClass {

  val ctt = "tata"

  override val conf = ctt
}

trait SuperClassFail {

  val conf: String

  // Fail because finalConf will be initialized before conf is overwritten and is therefore null.
  val finalConf = s"toto$conf"
}

class ImplementedClassFail extends SuperClassFail {

  val ctt = "tata"

  override val conf = ctt
}

abstract class AbstractClassFail {

  val conf: String

  // Fail because finalConf will be initialized before conf is overwritten and is therefore null.
  val finalConf = s"toto$conf"
}

class ImplementedAbstractClassFail extends AbstractClassFail {

  val ctt = "tata"

  override val conf = ctt
}