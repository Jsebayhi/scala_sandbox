package sandbox.scalapl.`lazy`

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

trait Lazy[T] {
  def act[U](f: T => U): (Lazy[T], U)
  def actIfInitialized[U](f: T => U): (Lazy[T], Option[U])
  def isInitialized: Boolean
}

case class UnInitializedLazy[T](builder: () => T) extends Lazy[T] {
  override def act[U](f: T => U): (Lazy[T], U) = {
    InitializedLazy(builder()).act(f)
  }

  override def actIfInitialized[U](f: T => U): (Lazy[T], Option[U]) = {
    (this, None)
  }

  override def isInitialized: Boolean = false
}

case class InitializedLazy[T](thing: T) extends Lazy[T] {
  override def act[U](f: T => U): (Lazy[T], U) = {
    (this, f(thing))
  }

  override def actIfInitialized[U](f: T => U): (Lazy[T], Option[U]) = {
    (this, Some(f(thing)))
  }

  override def isInitialized: Boolean = true
}

class example extends AnyFlatSpec with Matchers {

  it should "initialize and act" in {
    val lazyBob: Lazy[String] = UnInitializedLazy(() => "Bob")
    val (bob, upperBob) = personToUpper(lazyBob)

    upperBob shouldBe "BOB"

    bob.isInitialized shouldBe true
  }

  it should "act since it was initialized" in {
    val lazyBob: Lazy[String] = UnInitializedLazy(() => "Bob")
    val (bob, upperBob) = personToUpper(lazyBob)

    var res: Boolean = false

    upperBob shouldBe "BOB"

    bob.isInitialized shouldBe true
    bob.actIfInitialized(_ => res = true)

    res shouldBe true
  }

  it should "not act since it was not initialized" in {
    val lazyBob: Lazy[String] = UnInitializedLazy(() => "Bob")

    var res: Boolean = false

    lazyBob.isInitialized shouldBe false
    lazyBob.actIfInitialized(_ => res = true)

    res shouldBe false
  }

  def personToUpper(person: Lazy[String]): (Lazy[String], String) = {
    // Here you will initialize it and do the computing you want.
    // The interest is that you will not need to know how to instanciate Bob
    // since it was defined before, you just do your computations and return a state.
    person.act(p => p.toUpperCase)
  }
}