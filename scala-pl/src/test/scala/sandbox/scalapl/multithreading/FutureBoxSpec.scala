package sandbox.scalapl.multithreading

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.GivenWhenThen

class FutureBoxSpec extends AnyFlatSpec with Matchers with GivenWhenThen {

  it should "run the future then serve the result on demand" in {
    Given("a boxed future")
    val fb = new FutureBox[String, String](
      task = {
        Thread.sleep(1000)
        "toto"
      },
      onComplete = tried => tried.get
    )

    Then("Waiting for it to complete")
    fb.isCompleted shouldBe false
    fb.get shouldBe None
    Thread.sleep(1200)

    Then("asking for the value a first time")
    fb.isCompleted shouldBe true
    fb.get shouldBe Some("toto")

    Then("asking for the value a second time")
    fb.isCompleted shouldBe true
    fb.get shouldBe Some("toto")
  }
}
