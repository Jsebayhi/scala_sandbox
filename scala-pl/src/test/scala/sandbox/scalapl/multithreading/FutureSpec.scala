package sandbox.scalapl.multithreading

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Failure, Success }

class FutureSpec extends AnyFlatSpec with Matchers {

  def sleep(time: Long): Unit = { Thread.sleep(time) }

  "Future" should "work asynchronously" in {
    val maybeRes = new ThreadSafeMutableValue[Option[String]](None)

    val f = Future {
      sleep(1000)
      "I am Back"
    }
    f.onComplete {
      case Success(res) => maybeRes.set(Some(res))
      case Failure(e)   => throw e
    }
    maybeRes.get shouldBe None
    sleep(100)
    maybeRes.get shouldBe None
    sleep(400)
    maybeRes.get shouldBe None
    sleep(400)
    maybeRes.get shouldBe None
    sleep(100)
    sleep(200)
    maybeRes.get shouldBe Some("I am Back")
  }
}
