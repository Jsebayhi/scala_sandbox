package sandbox.scalapl

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.{ Failure, Success, Try }

class TrySpec extends AnyFlatSpec with Matchers {

  it should "not catch fatal" in {
    a[ThreadDeath] should be thrownBy Try(throw new ThreadDeath())
  }

  it should "not allow to silence fatal" in {
    a[ThreadDeath] should be thrownBy Try(throw new ThreadDeath()).recoverWith {
      case (e: Throwable) => Success(e.getMessage)
    }
  }

  it should "catch exceptions with recover" in {
    case class TheException() extends RuntimeException("toto")
    case class ItWorked() extends RuntimeException("coucou")

    val res = Try(throw TheException()).recoverWith({
      case _: RuntimeException => Failure(ItWorked())
      case d                   => Failure(new RuntimeException(s"we missed it SIR! ${d.toString}"))
    })

    res.isFailure shouldBe true
    res shouldBe Failure(ItWorked())
  }
}
