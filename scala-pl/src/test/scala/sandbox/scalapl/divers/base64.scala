package sandbox.scalapl.divers

import java.util.Base64.getDecoder

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.{ Success, Try }

class base64 extends AnyFlatSpec with Matchers {

  /**
   * An alternative would be import org.apache.commons.codec.binary.Base64.decodeBase64
   */
  "Base64 decoder" should "fail to decode base64 string with RFC4648 unsupported characters using java.util" in {
    val str = "AK0kHP1O-RgdgLSoWxkuaYoi5Jic6hLKeuKw8WzCfsQ68ntBDf6tVOTn_kZA7Gjf4oJAL1dXLlxIEy-kZWnxT3FF-0MQ4WQYbGBfaW8LTM4uAOLLvYZ8SIVEXmxhJsSlvaiTWCbNFaOfiII8bhFp4551YB07NfpquUGEwOxOmci_"
    Try(getDecoder.decode(str)).isFailure shouldBe true
  }
}
