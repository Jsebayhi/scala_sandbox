package sandbox.scalapl.divers

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

case class container(key1: String, key2: String, key3: String, value: Int)
case class containerMerged(key1: String, key2: String, value: Int)

object ContainerMerger {
  def containerKey2Merger(containers: List[container]): List[containerMerged] = {
    containers.groupBy { container => container.key1 }
      .flatMap {
        case (key1, containers) => containers.groupBy { container => container.key2 }
          .map {
            case (key2, containers) => containerMerged(
              key1 = key1,
              key2 = key2,
              value = containers.foldLeft(0) { (acc, in) => acc + in.value })
          }
      }.toList
  }
}

class ContainerMergerSpec extends AnyFlatSpec with Matchers {
  import ContainerMerger._
  "containerKey2Merger" should "be succesful" in {
    val containers = List(
      container(key1 = "pods", key2 = "toto", key3 = "a", value = 2),
      container(key1 = "pods", key2 = "toto", key3 = "b", value = 1),
      container(key1 = "pods", key2 = "titi", key3 = "c", value = 1),
      container(key1 = "notPods", key2 = "titi", key3 = "c", value = 2)
    )
    val containersMerged = List(
      containerMerged(key1 = "pods", key2 = "toto", value = 3),
      containerMerged(key1 = "pods", key2 = "titi", value = 1),
      containerMerged(key1 = "notPods", key2 = "titi", value = 2)
    )
    val res = containerKey2Merger(containers)
    containersMerged.foreach { containerMerged => res should contain(containerMerged) }
    containersMerged.length shouldBe res.length
  }
}

