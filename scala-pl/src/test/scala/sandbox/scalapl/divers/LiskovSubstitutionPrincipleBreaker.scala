package sandbox.scalapl.divers

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class LiskovSubstitutionPrincipleBreaker extends AnyFlatSpec with Matchers {

  it should "break the Liskov Substitution Principle" in {
    val liskovSubstitution: Liskov[LiskovSubstitution] = LiskovSubstitution("liskov", "substitution")

    giveMeLiskovBack(liskovSubstitution).castToSub.substitution shouldBe "substitution"
  }

  def giveMeLiskovBack[T <: Liskov[T]](liskov: Liskov[T]): Liskov[T] = {
    liskov
  }
}

abstract class Liskov[T] {
  def liskov: String

  type t = T

  def castToSub: t = this.asInstanceOf[t]
}

case class LiskovSubstitution(liskov: String, substitution: String) extends Liskov[LiskovSubstitution]