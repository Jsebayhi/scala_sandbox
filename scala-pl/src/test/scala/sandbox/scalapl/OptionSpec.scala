package sandbox.scalapl

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class OptionSpec extends AnyFlatSpec with Matchers {
  "Option" should "be None if given null" in {
    Option(null) shouldBe None
  }
}
