package sandbox.scalapl.`trait`.mixin

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class TraitMixinSpec extends AnyFlatSpec with Matchers {

  "TraitMixin" must "print B" in {
    object test1 extends A with B {}
    test1.toto shouldBe "B"
  }
  it must "print C" in {
    object test2 extends A with C {}
    test2.toto shouldBe "C"
  }
  it must "print AD" in {
    object test3 extends A with D {}
    test3.toto shouldBe "AD"
  }
  it must "print AEF" in {
    object test3 extends E with F {}
    test3.toto shouldBe "AEF"
  }
}

trait A {
  def toto = {
    "A"
  }
}

trait B {
  this: A =>
  override def toto = {
    "B"
  }
}

trait C extends A {
  override def toto = {
    "C"
  }
}

trait D extends A {
  override def toto = {
    super.toto + "D"
  }
}

trait E extends A {
  override def toto = {
    super.toto + "E"
  }
}

trait F extends A {
  override def toto = {
    super.toto + "F"
  }
}

