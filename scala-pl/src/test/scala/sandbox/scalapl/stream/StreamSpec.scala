package sandbox.scalapl.stream

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class StreamSpec extends AnyFlatSpec with Matchers {

  it should "not recompute common stream part" in {
    val s: LazyList[Int] = LazyList.from(0)

    val s2: LazyList[Int] = s.map(i => { println(s"first step. $i"); i }).take(10)

    s2.foreach(i => println(s"first branch: $i"))
    s2.foreach(i => println(s"second branch: $i"))
  }

}
