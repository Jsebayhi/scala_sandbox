package sandbox.scalapl.test

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.PrivateMethodTester
import scala.util.{ Try }

class AClass {
  private def intIdentity(i: Int) = i
}

object AClass {
  private def identity(s: String) = s
}

object Obj {
  private def identity(s: String) = s
}

class PrivateMethodTest extends AnyFlatSpec with Matchers with PrivateMethodTester {

  it should "test private method 'identity' of object obj" in {
    val identity = PrivateMethod[String](Symbol("identity"))
    val thing = "theThing"

    Obj invokePrivate identity(thing) shouldBe thing
  }

  it should "test private method 'identity' of companion object AClass" in {
    val identity = PrivateMethod[String](Symbol("identity"))
    val thing = "theThing"

    AClass invokePrivate identity(thing) shouldBe thing
  }

  it should "not find private method 'intIdentity' of class AClass in companion object AClass" in {
    val identity = PrivateMethod[Int](Symbol("intIdentity"))
    val intThing = 1

    Try(AClass invokePrivate identity(intThing) shouldBe intThing).isFailure shouldBe true
  }

  it should "test private method 'intIdentity' of class AClass in an instance of AClass" in {
    val identity = PrivateMethod[Int](Symbol("intIdentity"))
    val intThing = 1

    new AClass() invokePrivate identity(intThing) shouldBe intThing
  }
}
