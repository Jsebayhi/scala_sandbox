package sandbox.scalapl

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.{ MatchResult, Matcher }
import sandbox.scalapl.CustomMatcher.beCustomEquivalentTo

object CustomMatcher {
  class JsonMatcher(expectedJson: String) extends Matcher[String] {
    def apply(left: String) = {
      MatchResult(
        expectedJson == left,
        s"$left was not equivalent to $expectedJson",
        s"$left was equivalent to $expectedJson"
      )
    }
  }

  def beCustomEquivalentTo(expectedJson: String) = new JsonMatcher(expectedJson)
}

class CustomMatcherTest extends AnyFlatSpec with Matchers {
  import CustomMatcher._
  "customMatcher" should "work as intended and match" in {
    "toto" should beCustomEquivalentTo("toto")
  }
}