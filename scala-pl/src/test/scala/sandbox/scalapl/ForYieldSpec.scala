package sandbox.scalapl

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.{ Failure, Success }

class ForYieldSpec extends AnyFlatSpec with Matchers {

  "forYield" should "return a valid result" in {
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- Success("titi")
    } yield Triple(a, b, c)

    res shouldBe Success(Triple("toto", "tata", "titi"))
  }

  it should "return the failure" in {
    val failure = Failure(new RuntimeException("it can happen."))
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- failure
    } yield Triple(a, b, c)

    res shouldBe failure
  }

  it should "return the first failure" in {
    val firstFailure = Failure(new RuntimeException("first!"))
    val secondFailure = Failure(new RuntimeException("second!"))
    val res = for {
      a <- Success("toto")
      b <- firstFailure
      c <- secondFailure
    } yield Triple(a, b, c)

    res shouldBe firstFailure
  }

  it should "catch thrown exception" in {
    val failure = new RuntimeException("it can happen.")
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- { throw failure; Success("titi") }
    } yield Triple(a, b, c)
    res shouldBe Failure(failure)
  }

  it should "not ignore unused nested result" in {
    val failure = Failure(new RuntimeException("it can happen."))
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- Success("titi")
      d <- failure
    } yield Triple(a, b, c)

    res shouldBe failure
  }

  it should "return the result given satisfied condition in the yield part" in {
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- Success("titi")
      d <- Success(3)
    } yield if (d < 10) Some(Triple(a, b, c)) else None

    res shouldBe Success(Some(Triple("toto", "tata", "titi")))
  }

  it should "not return the result given unsatisfied condition in the yield part" in {
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- Success("titi")
      d <- Success(3)
    } yield if (d > 10) Some(Triple(a, b, c)) else None

    res shouldBe Success(None)
  }

  it should "return a result given a satisfied condition in the for part" in {
    val failure = Failure(new RuntimeException("condition unsatisfied"))
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- Success("titi")
      d <- if (c.length < 10) Success(c) else failure
    } yield Triple(a, b, d)

    res shouldBe Success(Triple("toto", "tata", "titi"))
  }

  it should "not return a result given an unsatisfied condition in the for part" in {
    val failure = Failure(new RuntimeException("condition unsatisfied"))
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- Success("titi")
      d <- if (c.length > 10) Success(c) else failure
    } yield Triple(a, b, d)

    res shouldBe failure
  }

  /**
   * [error] scala_sandbox/scala-pl/src/test/scala/sandbox/scalapl/ForYieldSpec.scala:57:
   * value map is not a member of RuntimeException
   * [error]       c <- failure
   * [error]            ^
   * [error] one error found
   */
  it should "not compile if you return directly the exception instance instead of a Failure" in {
    /*val failure = new RuntimeException("it can happen.")
    val res = for {
      a <- Success("toto")
      b <- Success("tata")
      c <- {failure; Success("titi")}
    } yield Triple(a, b, c)
    res shouldBe Not compile
    */
  }
}

case class Triple(a: String, b: String, c: String)