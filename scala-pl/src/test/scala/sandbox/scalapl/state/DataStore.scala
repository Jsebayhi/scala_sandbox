package sandbox.scalapl.state

object DefaultDataStore extends DataStore[String](List("toto", "tata", "titi", "tutu"))

class DataStore[T](data: List[T]) {
  type THIS = this.type

  var index = 0
  var previousIndex = 0

  def getThenUpdate(): T = {
    val d = get
    updateIndex()
    d
  }

  def get: T = data(index)

  private def updateIndex(): THIS = {
    previousIndex = index
    if (index + 1 == data.size) {
      index = 0
    } else {
      index += 1
    }
    this
  }

  def reset(): THIS = {
    index = 0
    this
  }
}