package sandbox.scalapl.state

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

object updateThing extends StateExecutor[Option[String], String]((state) => {
  val datum = DefaultDataStore.getThenUpdate()

  val s = state match {
    case Some(str) => str + " " + datum
    case None      => datum
  }

  (Some(s), s)
}, None)

class StateExecutorSpec extends AnyFlatSpec with Matchers {

  it should "work" in {
    DefaultDataStore.reset()

    updateThing.act() shouldBe "toto"
    updateThing.act() shouldBe "toto tata"
    updateThing.act() shouldBe "toto tata titi"
    updateThing.act() shouldBe "toto tata titi tutu"
  }
}

