package sandbox.scalapl.state

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.GivenWhenThen

class F_StateExecutorSpec extends AnyFlatSpec with Matchers with GivenWhenThen {

  "F_SimpleStateExecutor[T]" should "work" in {
    Given("DataStore")
    DefaultDataStore.reset()

    When("initializing a F_SimpleStateExecutor")
    val fse: F_SimpleStateExecutor[String] = new F_SimpleStateExecutor[String](DefaultDataStore.getThenUpdate(), str => str + ", " + DefaultDataStore.getThenUpdate())
    fse.state shouldBe "toto"

    Then("asking for an update")
    val fse_2: F_SimpleStateExecutor[String] = fse.update
    fse_2.state shouldBe "toto, tata"
  }

  "F_StateExecutor[S, C, V]" should "work" in {
    Given("DataStore")
    DefaultDataStore.reset()

    When("initializing a F_SimpleStateExecutor")
    val fse: F_StateExecutor[String, String, String] = {
      new F_StateExecutor[String, String, String](
        state = DefaultDataStore.getThenUpdate(),
        action = str => (c) => {
          val newFetch = DefaultDataStore.getThenUpdate()
          val s = str + c + newFetch
          val v = str + newFetch
          (s, v)
        }
      )
    }
    fse.state shouldBe "toto"

    Then("asking for an update")
    val (fse_2, v): (F_StateExecutor[String, String, String], String) = fse.act(", ")
    fse_2.state shouldBe "toto, tata"
    v shouldBe "tototata"
  }
}