package sandbox.scalapl.extractor

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

object Extractor {
  val Nastia = "Nastia"
  val NastiaNameCode = 1
  val NoOneNameCode = 0

  def apply(name: String) = {
    name match {
      case Nastia => NastiaNameCode
      case _      => NoOneNameCode
    }
  }

  //must be an Option
  def unapply(id: Int): Option[String] = {
    id match {
      case NastiaNameCode => Some(Nastia)
      case _              => None
    }
  }
}

class ExtractorSpec extends AnyFlatSpec with Matchers {
  "Extractor" should "get id from string" in {
    Extractor(Extractor.Nastia) shouldBe Extractor.NastiaNameCode
  }
  it should "get string from id" in {
    val Extractor(name) = Extractor.NastiaNameCode
    name shouldBe Extractor.Nastia
  }
  it should "throw an error for unrecognized id" in {
    intercept[scala.MatchError] {
      val Extractor(name) = 0
    }
  }
}

