package sandbox.scalapl.enum

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

object anEnum extends Enumeration {
  type anEnum = Value
  val key1, key2, key3 = Value

  def identify(uk: String): Option[anEnum] = values.find(_.toString == uk)
}

class EnumSpec extends AnyFlatSpec with Matchers {

  import anEnum._
  import thingsUsingAnEnum._

  "anEnum" should "work" in {
    val t = key1

    t should not be (key2)
  }

  it should "be usable in function" in {
    val t = key2
    isKey1(t) shouldBe false
  }

  it should "be printable as a string" in {
    val t = key2
    t.toString shouldBe "key2"
  }

  it should "identify a string as enum" in {
    identify("key2") shouldBe Some(key2)
  }

  it should "identify a string which is not an enum" in {
    identify("unknown") shouldBe None
  }
}

object thingsUsingAnEnum {

  def isKey1(key: anEnum.anEnum): Boolean = key == anEnum.key1
}