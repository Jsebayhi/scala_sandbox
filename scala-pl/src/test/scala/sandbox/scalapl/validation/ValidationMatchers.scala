package sandbox.scalapl.validation

import org.scalatest.matchers.{ MatchResult, Matcher }

object ValidationMatchers {
  def beAValidationError[T](expectedMsg: String) = new ErrorMsgMatcher[T](expectedMsg)
}

class ErrorMsgMatcher[T](expectedErrorMsg: String) extends Matcher[Either[ValidationError, T]] {
  def apply(left: Either[ValidationError, T]): MatchResult = {
    left match {
      case Left(err) => MatchResult(err.error == expectedErrorMsg, s"${err.error} was not equal to $expectedErrorMsg", "Error msg matched expected.")
      case r         => MatchResult(r.isLeft, s"${r.toString} was not a Left[ValidationError]", "")
    }
  }
}