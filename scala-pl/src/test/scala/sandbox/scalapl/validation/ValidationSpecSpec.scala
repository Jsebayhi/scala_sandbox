package sandbox.scalapl.validation

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import ValidationMatchers._

class ValidationSpecSpec extends AnyFlatSpec with Matchers {

  val positiveIntValidator = ValidationSpec[Int](enrichError = "not a positive int")
    .rule(_ < 0, "inferior to 0")

  val DatumValidator = ValidationSpec[Datum](enrichError = "not a valid Datum")
    .ruleU(_.str, BasicValidators.StringValidator, "invalid str")
    .ruleU(_.number, positiveIntValidator, "invalid number")

  "failFast" should "identify a bad Datum with a bad str" in {
    val res = DatumValidator.failFast(Datum("", -1))

    res should beAValidationError[Datum]("not a valid Datum, invalid str, not a valid string, empty")
  }

  it should "identify a valid Datum with a valid str" in {
    val datum = Datum("valid", 2)
    val res = DatumValidator.failFast(datum)

    if (res.isLeft) { println(res.swap.map(_.error).getOrElse("")) }
    res shouldBe Right(datum)
  }

  "fullScan" should "identify a bad Datum with a bad str and a bad number" in {
    val res = DatumValidator.fullScan(Datum("", -1))

    res should beAValidationError[Datum]("not a valid Datum, causes: [invalid str, not a valid string, empty; invalid number, not a positive int, inferior to 0]")
  }

  it should "identify a valid Datum with a valid str" in {
    val datum = Datum("valid", 2)
    val res = DatumValidator.fullScan(datum)

    res shouldBe Right(datum)
  }

}

case class Datum(str: String, number: Int)