package sandbox.scalapl.collection

class TrieMapWithHistory[K, V](val historySize: Int) {
  val container = scala.collection.concurrent.TrieMap[K, V]()
  var indexHistory = scala.collection.mutable.Queue[K]()

  def getOrElse(index: K, content: V): V = {
    container.getOrElse(index, {
      container += index -> content
      indexHistory += index
      if (indexHistory.size > historySize) {
        container.remove(indexHistory.dequeue())
      }
      content
    })
  }
}
