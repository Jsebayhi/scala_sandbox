package sandbox.scalapl.multithreading

class ThreadSafeMutableValue[T](ini: T) {
  type THIS = this.type

  private var value: T = ini

  def get: T = {
    this.synchronized {
      value
    }
  }

  def update(action: T => T): THIS = {
    this.synchronized {
      value = action(value)
    }
    this
  }

  def set(v: T): Unit = {
    this.synchronized {
      value = v
    }
  }

  def transform[U](action: T => U): U = {
    this.synchronized {
      action(value)
    }
  }
}