package sandbox.scalapl.multithreading

import scala.concurrent.Future
import scala.util.Try
import scala.concurrent.ExecutionContext.Implicits.global

class FutureBox[T, U](task: => T, onComplete: Try[T] => U) {

  private val maybeResult = new ThreadSafeMutableValue[Option[U]](None)

  private val future = Future[T](task)
  future.onComplete { tried =>
    maybeResult.set(Some(onComplete(tried)))
  }

  def isCompleted: Boolean = future.isCompleted

  def get: Option[U] = maybeResult.get
}