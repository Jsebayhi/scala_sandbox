package sandbox.scalapl.validator

case class Validator[T](private val checks: List[T => List[String]] = Nil) {
  def withValidation(f: T => List[String]): Validator[T] = {
    this.copy(checks = checks :+ f)
  }

  /**
   * Helper function to ease checks returning Either
   */
  def withEitherValidation[U](f: T => Either[String, U]): Validator[T] = {
    withValidation(t => f(t).swap.toOption.toList)
  }

  /**
   * Validate the given T and return every errors found.
   * @param t
   * @return
   */
  def validate(t: T): Either[List[String], T] = {
    checks.flatMap(f => f(t)) match {
      case Nil => Right(t)
      case e   => Left(e)
    }
  }
}
