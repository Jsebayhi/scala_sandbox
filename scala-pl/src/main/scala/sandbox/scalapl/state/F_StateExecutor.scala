package sandbox.scalapl.state

/*
*** A state executor the Functional way
 */

class F_SimpleStateExecutor[T](val state: T, stateUpdater: (T) => T) {

  type THIS = F_SimpleStateExecutor[T]

  def update: THIS = {
    val s = stateUpdater(state)

    if (s != state) {
      new THIS(s, stateUpdater)
    } else {
      this
    }
  }

}

/**
 *
 * @param state
 * @param action
 * @tparam S State
 * @tparam C Complement
 * @tparam V Value
 */
class F_StateExecutor[S, C, V](val state: S, action: S => C => (S, V)) {

  type THIS = F_StateExecutor[S, C, V]

  def act: C => (THIS, V) = {
    (c: C) =>
      {
        val (s, v) = action(state)(c)

        if (s != state) {
          (new THIS(s, action), v)
        } else {
          (this, v)
        }
      }
  }
}

