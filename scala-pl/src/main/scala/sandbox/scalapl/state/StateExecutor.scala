package sandbox.scalapl.state

/*
*** Object Oriented
 */

/*
class StateExecutor[K, V](loader: (Option[K]) => (Option[K], V), stateInitializer: Option[K]) {

  private var state: Option[K] = stateInitializer

  def load(): V = loader(state) match {
    case (s, res) => {
      state = s
      res
    }
  }
}
*/

trait WithStateExecutor[K, V] {
  type THIS = this.type

  protected val stateInitializer: K

  protected def action: K => (K, V)

  private var state: K = _

  protected def initializeState(): THIS = {
    state = stateInitializer
    this
  }

  def act(): V = {
    val (s, res) = action(state)
    state = s
    res
  }
}

class StateExecutor[K, V](actionOverride: K => (K, V), stateIni: K) extends WithStateExecutor[K, V] {

  override protected val stateInitializer = stateIni

  override protected def action = actionOverride

  initializeState()
}

object test {
  val ase = new SynchedStateExecutor[String, String, String]("", state => input => (state + input, state + input))

  val ase2 = new SynchedStateExecutor[Int, Int, Int](0, state => input => (state + input, state + input))

  val ase3 = new SynchedStateExecutor[Option[Int], Int, Int](None, state => input => (Some(state.getOrElse(0) + input), state.getOrElse(0) + input))
}
