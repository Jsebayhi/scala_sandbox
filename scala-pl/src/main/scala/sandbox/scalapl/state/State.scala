package sandbox.scalapl.state

class State[S](initialization: S) {
  private var state: S = initialization

  def updateState(v: S): Unit = {
    this.synchronized {
      state = v
    }
  }

  def getState: S = {
    this.synchronized {
      state
    }
  }
}
