package sandbox.scalapl.state

/**
 * Synchronized State Executor
 * @param stateIni value to which initialize the state
 * @param action function to execute taking the state, a complementary input and
 *               returning the state potentially updated and a computed value.
 * @param manageState function run at instantiation which allow the user to
 *                    manage the state.
 *                    As an example, if used as a cache it allow to set up a
 *                    thread which clean the cache from time to time.
 * @tparam S State
 * @tparam C Complement
 * @tparam V Value
 */
class SynchedStateExecutor[S, C, V](
    stateIni: S,
    action: S => C => (S, V),
    manageState: State[S] => Unit = (_: State[S]) => {}) {

  type THIS = SynchedStateExecutor[S, C, V]

  private val state: State[S] = new State[S](stateIni)

  manageState(state)

  def getState: S = state.getState

  def resetState(stateOverride: S = stateIni): THIS = {
    this.synchronized {
      state.updateState(stateOverride)
    }
    this
  }

  def act: C => (THIS, V) = {
    (c: C) =>
      {
        this.synchronized {
          val (s, v): (S, V) = action(state.getState)(c)

          if (s != state.getState) {
            state.updateState(s)
          }
          (this, v)
        }
      }
  }
}