package sandbox.scalapl.error

/**
 * Handle exception's full message extraction boilerplate.
 */
trait ThrowableErrorCause {
  self: AbsError =>

  def cause: Throwable
  def context: String

  override lazy val error: String = ThrowableErrorCause.extractMessageWithCause(cause)
    .map(context + ", " + _)
    .getOrElse("")
}

object ThrowableErrorCause {
  def extractMessageWithCause(t: Throwable): Option[String] = {
    Option(t.getMessage)
      .map(_ + extractCauseMessageRec(t).map(" Cause: " + _ + ".").getOrElse(""))
  }

  def extractCauseMessageRec(t: Throwable): Option[String] = {
    def inner(t: Throwable, acc: Option[String]): Option[String] = {
      extractCauseMessage(t) match {
        case None      => acc
        case Some(msg) => inner(t.getCause, Option(acc.map(_ + ", " + msg).getOrElse(msg)))
      }
    }
    inner(t, None)
  }

  def extractCauseMessage(t: Throwable): Option[String] = {
    Option(t.getCause)
      .map(_.getMessage)
  }
}