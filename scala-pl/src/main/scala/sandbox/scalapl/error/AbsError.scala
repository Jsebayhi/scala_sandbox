package sandbox.scalapl.error

/**
 * Abstract error.
 *
 * The highest level error abstraction.
 */
trait AbsError {
  def error: String
}

/**
 * Handle nested error boilerplate.
 */
trait NestedError {
  self: AbsError =>

  def cause: AbsError
  def context: String

  override lazy val error: String = context + Option(cause.error).filter(_.nonEmpty).map(", " + _).getOrElse("")
}
