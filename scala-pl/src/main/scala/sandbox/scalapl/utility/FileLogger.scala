package com.carrefour.phenix.logistic.infolog.normalizer

/*
** THIS IS FOR DEBUGGING ONLY AND STILL FOR THAT IT IS DIRTY!
*/

case class Level(Name: String, value: Int) {
  def isInferiorTo(level: Level): Boolean = this.value < level.value
  def isInferiorOrEqualTo(level: Level): Boolean = this.value <= level.value
  def isSuperiorTo(level: Level): Boolean = this.value > level.value
  def isSuperiorOrEqualTo(level: Level): Boolean = this.value >= level.value
}

object Levels {

  private var levelValue: Int = 0

  private def getLevelValueAndIncrement(): Int = {
    val lvl = levelValue
    levelValue += 1
    lvl
  }

  val Debug = Level("Debug", getLevelValueAndIncrement())
  val Info = Level("Info", getLevelValueAndIncrement())
  val Warn = Level("Warn", getLevelValueAndIncrement())
  val Error = Level("Error", getLevelValueAndIncrement())
}

/**
 * @note NotThreadSafe
 */
object FileLoggerSingleton {
  import Levels._

  type This = this.type

  private var doesStateChanged: Boolean = true

  private def setDoesStateChanged(): Unit = this.doesStateChanged = true

  private def resetDoesStateChanged(): Unit = this.doesStateChanged = false

  private var level = Debug

  def setLevel(lvl: Level): This = {
    this.level = lvl
    this.setDoesStateChanged()
    this
  }

  // FilePath

  private var filePath: Option[String] = None
  this.resetFilePath()

  private def isFilePathValid(filePath: String): Boolean = Option(filePath).isDefined

  def resetFilePath(): Unit = {
    this.filePath = None
    this.setDoesStateChanged()
  }

  def setFilePath(filePath: String): This = {
    if (isFilePathValid(filePath)) {
      this.filePath = Some(filePath)
      this.setDoesStateChanged()
    } else {
      this.resetFilePath()
    }
    this
  }

  // FileName

  private var fileName: Option[String] = None
  this.resetFileName()

  private def isFileNameValid(fileName: String): Boolean = Option(fileName).isDefined

  def resetFileName(): Unit = {
    this.fileName = None
    this.setDoesStateChanged()
  }

  def setFileName(fileName: String): This = {
    if (isFileNameValid(fileName)) {
      this.fileName = Some(fileName)
      this.setDoesStateChanged()
    } else {
      this.resetFileName()
    }
    this
  }

  // FileFullPath

  private def fileFullPath: Option[String] = {
    if (filePath.isDefined && fileName.isDefined) {
      Some(filePath.get + "/" + fileName.get)
    } else {
      None
    }
  }

  def setFileFullPath(fileFullPath: String): This = {
    if (Option(fileFullPath).isDefined) {
      val tmp = fileFullPath.split("/")
      val fileName = tmp.last
      val filePath = tmp.dropRight(1).mkString("/")
      if (isFilePathValid(filePath) && isFileNameValid(fileName)) {
        this.setFilePath(filePath)
        this.setFileName(fileName)
      } else {
        this.resetFilePath()
        this.resetFileName()
      }
    }
    this
  }

  // FileLoggerInstance

  private var fileLoggerInstance: Option[FileLogger] = None
  this.resetFileLoggerInstance()

  private def resetFileLoggerInstance(): Unit = fileLoggerInstance = None

  private def updateFileLoggerInstance(): Unit = {
    (this.fileFullPath, this.doesStateChanged) match {
      case (Some(fileFullPath), true) =>
        this.fileLoggerInstance = Some(new FileLogger(this.level, fileFullPath))
        this.resetDoesStateChanged()
      case (None, true) => this.resetFileLoggerInstance()
      case _            =>
    }
  }

  def act(action: (FileLogger) => Unit): This = {
    if (doesStateChanged) { this.updateFileLoggerInstance() }
    this.fileLoggerInstance match {
      case None             =>
      case Some(fileLogger) => action(fileLogger)
    }
    this
  }
}

class FileLogger(level: Level, filePath: String) {

  private val (filePrinter, bufferWriter) = getFilePrinter(filePath)

  import Levels._
  def debug(msg: String) = logIfLevel(msg, Debug)
  def info(msg: String) = logIfLevel(msg, Info)
  def warn(msg: String) = logIfLevel(msg, Warn)
  def error(msg: String) = logIfLevel(msg, Error)

  private def logIfLevel(msg: String, lvl: Level): Unit = {
    if (level.isInferiorOrEqualTo(lvl)) {
      log(lvl.Name + ": " + msg)
    }
  }

  private def log(msg: String) = filePrinter.append(msg + "\n").flush()

  import java.io.{ PrintWriter, BufferedWriter }

  private def getFilePrinter(filePath: String): (PrintWriter, BufferedWriter) = {
    def initializeFile(filePath: String) = {
      import java.io.{ File, FileOutputStream, PrintWriter }

      val f1 = new File(filePath)
      val fos1 = new FileOutputStream(f1)
      val writer1 = new PrintWriter(fos1)
      writer1.write("")
      writer1.close()
      fos1.close()
    }

    initializeFile(filePath)

    import java.io.{ FileWriter, BufferedWriter, PrintWriter }

    val fw = new FileWriter(filePath, true)
    val bw = new BufferedWriter(fw)
    (new PrintWriter(bw), bw)
  }

  Runtime.getRuntime.addShutdownHook(new Thread() {
    override def run(): Unit = {
      filePrinter.close()
      bufferWriter.close()
    }
  })
}
