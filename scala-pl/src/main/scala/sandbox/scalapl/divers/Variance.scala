package sandbox.scalapl.divers

object Variance {
  class Animal {}
  class Mammal extends Animal {}
  class Dog extends Mammal {}
  class DogPuppy extends Dog {}

  /*covariant in T*/
  class ClassCovariant[+T] {}

  /*contravariant in T*/
  class ClassContravariant[-T] {}

  /*invariant in T*/
  class ClassInvariant[T] {}

  /*
  **** COVARIANCE
  *** Subtypes of Mammal and subTypes of Mammal's subType will work
   */
  def method1(box: ClassCovariant[Mammal]): Unit = {}
  //  method1(new Box1[Animal]) //compile fails
  method1(new ClassCovariant[Mammal])
  method1(new ClassCovariant[Dog])
  method1(new ClassCovariant[DogPuppy])

  /*
  **** CONTRAVARIANCE
  *** Only SuperType of Dog and SuperTypes of Dog's SuperType will work
   */
  def method2(box: ClassContravariant[Dog]): Unit = {}
  method2(new ClassContravariant[Animal])
  method2(new ClassContravariant[Mammal])
  method2(new ClassContravariant[Dog])
  //  method2(new Box2[DogPuppy]) //compile fails

  /*
  **** INVARIANCE
  *** Only object of type Mammal will work
   */
  def method3(box: ClassInvariant[Mammal]): Unit = {}
  //  method3(new Box3[Animal]) //compile fails
  method3(new ClassInvariant[Mammal])
  //  method3(new Box3[Dog]) //compile fails
}
