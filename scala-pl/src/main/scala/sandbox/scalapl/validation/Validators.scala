package sandbox.scalapl.validation

import sandbox.scalapl.scalax.RichEither.toRichEither

/**
 * Never returns an error.
 * @tparam T
 */
case class NoopValidator[T]() extends Validator[T] {
  override def failFast(thing: T): Either[ValidationError, T] = Right(thing)

  override def fullScan(thing: T): Either[ScanVError, T] = Right(thing)

  override def include(validator: Validator[T]*): Validator[T] = {
    ValidationSpec(checks = validator.toList, enrichError = identity)
  }
}

case class Rule[T](
    predicate: T => Boolean,
    error: ValidationError) extends Validator[T] {

  private def check(thing: T): Either[ValidationError, T] = {
    if (predicate(thing)) {
      Left(error)
    } else {
      Right(thing)
    }
  }

  override def failFast(thing: T): Either[ValidationError, T] = {
    check(thing)
  }

  override def fullScan(thing: T): Either[ScanVError, T] = {
    check(thing).left.map(err => ScanVError(identity, List(err)))
  }

  override def include(validator: Validator[T]*): Validator[T] = {
    this.copy(error = NoopVError)
      .merge(
        enrichError = err => AnonymousNestedVError(cause = err, error.error),
        validator = validator.toList: _*)
  }
}

/**
 * A list of validation to check.
 * @param checks
 * @tparam T
 */
case class ValidationSpec[T](
    enrichError: ValidationError => ValidationError,
    checks: List[Validator[T]] = Nil) extends Validator[T] {

  override def failFast(thing: T): Either[ValidationError, T] = {
    def innerFailFast(remainingChecks: List[Validator[T]]): Either[ValidationError, T] = {
      remainingChecks match {
        case head :: tail => head.failFast(thing) match {
          case Right(_)  => innerFailFast(tail)
          case Left(err) => Left(enrichError(err))
        }
        case Nil => Right(thing)
      }
    }
    innerFailFast(checks)
  }

  override def fullScan(thing: T): Either[ScanVError, T] = {
    val errors: List[ValidationError] = checks.flatMap(check => check.fullScan(thing).swap.toOption)
    if (errors.nonEmpty) {
      Left(ScanVError(enrichError, errors))
    } else {
      Right(thing)
    }
  }

  override def include(validator: Validator[T]*): Validator[T] = {
    this.copy(checks = checks ++ validator)
  }
}

/**
 * A validation to apply given a valid predicate.
 * @param given
 * @tparam T
 */
case class GivenValidator[T](
    given: T => Validator[T],
    enrichError: ValidationError => ValidationError) extends Validator[T] {

  override def failFast(thing: T): Either[ValidationError, T] = {
    given(thing)
      .failFast(thing)
  }

  override def fullScan(thing: T): Either[ScanVError, T] = {
    given(thing)
      .fullScan(thing)
  }

  override def include(validator: Validator[T]*): Validator[T] = {
    ValidationSpec(
      enrichError = enrichError,
      checks = validator.toList :+ this.copy(enrichError = identity)
    )
  }
}

/**
 * A validation to apply given a valid previous Validation.
 */
case class GivenSuccessfulValidation[T](
    mustSuccess: Validator[T],
    givenSuccess: Validator[T],
    enrichError: ValidationError => ValidationError) extends Validator[T] {

  override def failFast(thing: T): Either[ValidationError, T] = {
    mustSuccess.failFast(thing)
      .flatMap(_ => givenSuccess.failFast(thing))
  }

  override def fullScan(thing: T): Either[ScanVError, T] = {
    mustSuccess.fullScan(thing)
      .flatMap(_ => givenSuccess.fullScan(thing))
  }

  override def include(validator: Validator[T]*): Validator[T] = {
    ValidationSpec(
      enrichError = enrichError,
      checks = validator.toList :+ this.copy(enrichError = identity)
    )
  }
}

/**
 * A validator adapted in order to be able to validate a type derived from
 * another type.
 * @param inputAdaptor
 * @param enrichError
 * @param innerValidator
 * @tparam T
 * @tparam U
 */
case class ProjectedValidator[T, U](
    inputAdaptor: T => U,
    enrichError: ValidationError => ValidationError = identity,
    innerValidator: Validator[U]) extends Validator[T] {
  override def failFast(thing: T): Either[ValidationError, T] = {
    innerValidator.failFast(inputAdaptor(thing))
      .fold(err => Left(enrichError(err)), _ => Right(thing))
  }

  override def fullScan(thing: T): Either[ScanVError, T] = {
    innerValidator.fullScan(inputAdaptor(thing))
      .fold(err => Left(err.copy(enrichError = e => enrichError(err.enrichError(e)))), _ => Right(thing))
  }

  override def include(validator: Validator[T]*): Validator[T] = {
    ValidationSpec(
      enrichError = enrichError,
      checks = validator.toList :+ this.copy(enrichError = identity)
    )
  }
}