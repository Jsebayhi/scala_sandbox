package sandbox.scalapl.validation.report

sealed trait ReportAst

case class AnonymousMultiCausesReport(causes: List[ReportAst]) extends ReportAst
case class MultiCausesReport(info: String, causes: List[ReportAst]) extends ReportAst
case class NestedReport(info: String, cause: ReportAst) extends ReportAst
case class RootReport(error: String) extends ReportAst
case class NoopReport() extends ReportAst

object ReportAst {
  /*
  import io.circe.Encoder
  import io.circe.generic.auto._
  import io.circe.syntax._

  val encodeReportAst: Encoder[ReportAst] = Encoder.instance {
    case report: MultiCausesReport          => report.asJson
    case report: AnonymousMultiCausesReport => report.asJson
    case report: NestedReport               => report.asJson
    case report: RootReport                 => report.asJson
    case report: NoopReport                 => report.asJson
  }

  def toJson(report: ReportAst): String = {
    report.asJson(encodeReportAst).noSpaces
  }
*/

  def asString(report: ReportAst): String = {
    report match {
      case AnonymousMultiCausesReport(causes) => causes.map(asString).mkString("; ")
      case MultiCausesReport(info, causes)    => info + formatNonEmptyString(s => s", causes: [$s]", causes.map(asString).mkString("; "))
      case NestedReport(info, cause)          => info + formatNonEmptyString(s => ", " + s, asString(cause))
      case RootReport(error)                  => error
      case NoopReport()                       => ""
    }
  }

  private def formatNonEmptyString(strBuilder: String => String, str: String): String = {
    Option(str).filter(_.nonEmpty).map(strBuilder).getOrElse("")
  }
}