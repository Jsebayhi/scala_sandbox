package sandbox.scalapl.validation

trait SealedValidator[T] extends FailFastValidator[T]
  with FullScanValidator[T]

trait FailFastValidator[T] {
  /**
   * Validate the thing.
   * Stop at the first encountered error and report it.
   *
   * @param thing
   * @return
   */
  //TODO: return a T tagged as Validated
  def failFast(thing: T): Either[ValidationError, T]
}

trait FullScanValidator[T] {
  /**
   * Validate the thing.
   * Execute every validation and reports every errors found.
   *
   * @param thing
   * @return a either[ScanVError, T] in order to be able to access directly the
   *         error list without pattern matching.
   */
  //TODO: return a T tagged as Validated
  def fullScan(thing: T): Either[ScanVError, T]
}

trait Validator[T] extends SealedValidator[T] {

  def seal: SealedValidator[T] = this

  /**
   * Will include the provided validator as a part of the current validator.
   */
  def include(validator: Validator[T]*): Validator[T]

  /*
  *** Syntactic sugars
   */

  /**
   * Syntactic sugar.
   *
   * Will merge the provided validator with the current validator and use the
   * provided enrichError.
   */
  def merge(
    enrichError: ValidationError => ValidationError,
    validator: Validator[T]*): Validator[T] = {
    ValidationSpec(
      enrichError = enrichError,
      checks = validator.toList :+ this
    )
  }

  /**
   * Syntactic sugar.
   *
   * Project this validator in order for it to be applied on U from T.
   */
  def project[U](
    inputAdaptor: U => T,
    enrichError: ValidationError => ValidationError): Validator[U] = {
    ProjectedValidator(inputAdaptor, enrichError, this)
  }

  /**
   * Syntactic sugar
   *
   * Do a conditional combination based on some information available on T.
   */
  def combineGiven(given: T => Validator[T]): Validator[T] = {
    include(GivenValidator(given, identity))
  }

  /**
   * Syntactic sugar
   *
   * Add a Rule to the validator.
   */
  def rule(predicate: T => Boolean, error: ValidationError): Validator[T] = {
    include(Rule(predicate, error))
  }

  /**
   * Syntactic sugar
   * Declare a validator to be used on an adapted data (example: a field of a
   * case class).
   */
  def ruleU[U](
    inputAdaptor: T => U,
    validator: Validator[U],
    enrichError: ValidationError => ValidationError): Validator[T] = {
    include(ProjectedValidator(inputAdaptor, enrichError, validator))
  }

  /**
   * Syntactic sugar
   * Declare a validator to be used on an adapted data (example: a field of a
   * case class).
   */
  def ruleU[U](
    inputAdaptor: T => U,
    enrichError: ValidationError => ValidationError)(subValidator: Validator[U] => Validator[U]): Validator[T] = {
    ruleU[U](inputAdaptor, subValidator(ValidationSpec[U](enrichError = identity)), enrichError)
  }

  /**
   * Syntactic sugar
   * Declare a validation to apply given a valid previous Validation. (example:
   * a field of a case class of a certain type given a validation).
   */
  def ruleGiven(
    validator: Validator[T],
    enrichError: ValidationError => ValidationError)(subValidator: Validator[T] => Validator[T]): Validator[T] = {
    include(GivenSuccessfulValidation(
      mustSuccess = validator,
      givenSuccess = subValidator(ValidationSpec(enrichError = identity)),
      enrichError = enrichError)
    )
  }

  /**
   * Syntactic sugar
   * Declare a validation to apply given a valid rule. (example:
   * a field of a case class of a certain type given a validation).
   */
  def ruleGiven(
    predicate: T => Boolean,
    error: ValidationError,
    enrichError: ValidationError => ValidationError)(subValidator: Validator[T] => Validator[T]): Validator[T] = {
    ruleGiven(Rule(predicate, error), enrichError)(subValidator)
  }
}
