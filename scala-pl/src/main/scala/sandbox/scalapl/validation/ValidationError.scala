package sandbox.scalapl.validation

import sandbox.scalapl.error.{ AbsError, NestedError }
import sandbox.scalapl.validation.report.{ AnonymousMultiCausesReport, MultiCausesReport, NestedReport, NoopReport, ReportAst, RootReport }

trait ValidationError extends AbsError {
  def produceReport: ReportAst

  override def error: String = ReportAst.asString(produceReport)
}

/**
 * Eliminate report production boilerplate.
 */
trait NestedValidationError extends ValidationError with NestedError {
  override def cause: ValidationError

  override lazy val produceReport: ReportAst = NestedReport(context, cause.produceReport)
}

/**
 * Syntactic sugar for validation error.
 */
object ValidationError {
  import scala.language.implicitConversions

  implicit def anonymousVError(str: String): ValidationError = {
    AnonymousVError(str)
  }

  implicit def enrichAnonymously(str: String): ValidationError => ValidationError = {
    cause => AnonymousNestedVError(cause, str)
  }
}

case object NoopVError extends ValidationError {
  override lazy val produceReport: ReportAst = NoopReport()
}

case class AnonymousVError(context: String) extends ValidationError {
  override def produceReport: ReportAst = RootReport(context)
}

case class AnonymousNestedVError(cause: ValidationError, context: String) extends NestedValidationError

case class ScanVError(enrichError: ValidationError => ValidationError, errors: List[ValidationError]) extends ValidationError {
  override lazy val produceReport: ReportAst = {
    val context = enrichError(NoopVError).error
    (context.isEmpty, errors.map(_.produceReport)) match {
      case (true, head :: Nil)  => head
      case (true, lst)          => AnonymousMultiCausesReport(lst)
      case (false, head :: Nil) => NestedReport(info = context, head)
      case (false, lst)         => MultiCausesReport(info = context, lst)
    }
  }
}