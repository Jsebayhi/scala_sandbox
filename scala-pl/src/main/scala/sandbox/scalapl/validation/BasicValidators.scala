package sandbox.scalapl.validation

object BasicValidators {
  import ValidationError._

  val StringValidator = ValidationSpec[String](enrichError = "not a valid string")
    .ruleGiven(
      Rule(predicate = _ == null, error = "null"),
      enrichError = identity) { v =>
        v.rule(_.trim.isEmpty, "empty")
      }
}