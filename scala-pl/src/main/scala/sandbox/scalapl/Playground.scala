package sandbox.scalapl

import scala.util.{ Failure, Success, Try }

object playground {
  def main(args: Array[String]): Unit = println(itGracefullyBreaks.get)

  private def itGracefullyBreaks = Try(itBreaks) match {
    case Success(t) => Success(t)
    case Failure(e) => Failure(ACleanException(e))
  }

  private def itBreaks: String = throw new RuntimeException("I am an exception")
}

case class ACleanException(e: Throwable) extends RuntimeException("a clean exception", e)

