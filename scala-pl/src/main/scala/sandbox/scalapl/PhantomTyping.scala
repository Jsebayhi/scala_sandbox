package sandbox.scalapl

object Tag {
  sealed trait Tag[T]
  type Tagged[A, T] = A with Tag[T]

  def apply[A, T](x: A): Tagged[A, T] = x.asInstanceOf[Tagged[A, T]]
}
