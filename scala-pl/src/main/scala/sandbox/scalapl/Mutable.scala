package sandbox.scalapl

class Mutable[T] {

  type THIS = this.type

  private var value: Option[T] = None

  def set(v: T): THIS = {
    value = Option(v)
    this
  }

  def get: Option[T] = value

  def getOrSet(v: => T): T = {
    if (value.isDefined) {
      value.get
    } else {
      value = Option(v)
      v
    }
  }

  def isEmpty = value.isEmpty
  def isDefined = value.isDefined
  def foreach[U](f: T => U): Unit = value.foreach(f)
  def transform(f: T => T): THIS = {
    value.foreach(v => value = Some(f(v)))
    this
  }
  def clear(): THIS = {
    value = None
    this
  }
}
