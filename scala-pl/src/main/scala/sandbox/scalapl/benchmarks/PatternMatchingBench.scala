package sandbox.scalapl.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

//  sbt "project scala-pl" "jmh:run sandbox.scalapl.benchmarks.PatternMatching -wi 10 -i 5 -f1 -t1"
/*
wi: 20 i:20
[info] Benchmark                Mode  Cnt       Score      Error   Units
[info] PatternMatchingBench.incorporatedIf  thrpt   20  198770.241 ± 5712.745  ops/ms
[info] PatternMatchingBench.matchThenIf  thrpt   20  184387.923 ± 2897.627  ops/ms
*/

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class PatternMatchingBench {
  import sandbox.scalapl.benchmarks.models.SimpleCaseClass

  val test = SimpleCaseClass("toto")

  @Benchmark
  def incorporatedIf = {
    test match {
      case SimpleCaseClass(str) if str == "tata" => str
      case SimpleCaseClass(str) if str == "toto" => str
      case SimpleCaseClass(str)                  => str
      case _                                     => ""
    }
  }

  @Benchmark
  def matchThenIf = {
    test match {
      case SimpleCaseClass(str) =>
        if (str == "tata") { str }
        else if (str == "toto") { str }
        else ""
      case _ => ""
    }
  }
}
