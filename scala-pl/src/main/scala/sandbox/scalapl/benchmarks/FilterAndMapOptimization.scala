package sandbox.scalapl.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

//  sbt "project scala-pl" "jmh:run sandbox.scalapl.benchmarks.FilterAndMapOptimizationBench -wi 10 -i 5 -f1 -t1"
/*
wi: 10 i:10

with 100 data:
[info] Benchmark                                             Mode  Cnt     Score    Error   Units
[info] PlaygroundBench.instanciate101CaseClassInstanciated  thrpt   10  1145.374 ± 10.151  ops/ms
[info] PlaygroundBench.filter100ThenTransformToCaseClass    thrpt   10   461.393 ±  8.369  ops/ms
[info] PlaygroundBench.transformToCaseClassThenfilter100    thrpt   10   416.552 ± 18.734  ops/ms

with 10.000 data:
[info] Benchmark                                               Mode  Cnt   Score   Error   Units
[info] PlaygroundBench.instanciate10001CaseClassInstanciated  thrpt   10  11.950 ± 0.097  ops/ms
[info] PlaygroundBench.filter10000ThenTransformToCaseClass    thrpt   10   3.982 ± 0.025  ops/ms
[info] PlaygroundBench.transformToCaseClassThenfilter10000    thrpt   10   3.530 ± 0.180  ops/ms
[info] PlaygroundBench.filter1000ThenTransformToCaseClass     thrpt   10   6.235 ± 0.068  ops/ms
[info] PlaygroundBench.transformToCaseClassThenfilter1000     thrpt   10   3.115 ± 0.027  ops/ms
[info] PlaygroundBench.filter100ThenTransformToCaseClass      thrpt   10   4.039 ± 0.794  ops/ms
[info] PlaygroundBench.transformToCaseClassThenfilter100      thrpt   10   3.356 ± 0.548  ops/ms

with 100.000 data:
[info] Benchmark                                               Mode  Cnt  Score   Error   Units
[info] PlaygroundBench.instanciate100001CaseClassInstanciated thrpt   10  0.886 ± 0.038  ops/ms
[info] PlaygroundBench.filter10000ThenTransformToCaseClass    thrpt   10  0.492 ± 0.013  ops/ms
[info] PlaygroundBench.transformToCaseClassThenfilter10000    thrpt   10  0.273 ± 0.007  ops/ms
[info] PlaygroundBench.filter1000ThenTransformToCaseClass     thrpt   10  0.393 ± 0.120  ops/ms
[info] PlaygroundBench.transformToCaseClassThenfilter1000     thrpt   10  0.223 ± 0.003  ops/ms
[info] PlaygroundBench.filter100ThenTransformToCaseClass      thrpt   10  0.380 ± 0.082  ops/ms
[info] PlaygroundBench.transformToCaseClassThenfilter100      thrpt   10  0.232 ± 0.007  ops/ms
 */
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class FilterAndMapOptimizationBench {
  import sandbox.scalapl.benchmarks.models.SimpleCaseClass

  val inputList: List[String] = {
    val lst = for { i <- 0 to 100000 } yield i
    lst.toList.map(_.toString)
  }

  @Benchmark
  def instanciate100001CaseClassInstanciated = inputList.map(str => SimpleCaseClass(str))

  @Benchmark
  def filter100ThenTransformToCaseClass = inputList.filter { str => str.toInt < 100 }.map(str => SimpleCaseClass(str))

  @Benchmark
  def transformToCaseClassThenfilter100 = inputList.map(str => SimpleCaseClass(str)).filter { scc => scc.aValue.toInt < 100 }

  @Benchmark
  def filter1000ThenTransformToCaseClass = inputList.filter { str => str.toInt < 1000 }.map(str => SimpleCaseClass(str))

  @Benchmark
  def transformToCaseClassThenfilter1000 = inputList.map(str => SimpleCaseClass(str)).filter { scc => scc.aValue.toInt < 1000 }

  @Benchmark
  def filter10000ThenTransformToCaseClass = inputList.filter { str => str.toInt < 10000 }.map(str => SimpleCaseClass(str))

  @Benchmark
  def transformToCaseClassThenfilter10000 = inputList.map(str => SimpleCaseClass(str)).filter { scc => scc.aValue.toInt < 10000 }
}