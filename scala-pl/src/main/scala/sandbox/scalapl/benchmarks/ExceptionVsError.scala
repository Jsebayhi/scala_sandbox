package sandbox.scalapl.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import scala.util.{ Try, Failure }

//  sbt "project scala-pl" "jmh:run sandbox.scalapl.benchmarks.ExceptionVsError -wi 10 -i 5 -f1 -t1"
/*
without any action before:
wi: 10 i:10
[info] Benchmark                                                        Mode  Cnt       Score      Error   Units
[info] ExceptionVsError.ErrorAsEitherLeft                              thrpt   10  267151.330 ± 1314.935  ops/ms
[info] ExceptionVsError.ExceptionThrowingAndCatching                   thrpt   10    1109.972 ±   75.398  ops/ms
[info] ExceptionVsError.ExceptionThrowingAndCatchingWithoutStackTrace  thrpt   10   28585.077 ± 2989.293  ops/ms
[info] ExceptionVsError.ExceptionThrowingWithCause                     thrpt   10    1008.325 ±  189.082  ops/ms
[info] ExceptionVsError.FailureException                               thrpt   10    1152.569 ±  182.847  ops/ms
[info] ExceptionVsError.Retrowing                                      thrpt   10   36022.249 ±  454.503  ops/ms

with action before:
wi: 10 i:10
[info] Benchmark                                                        Mode  Cnt       Score      Error   Units
[info] ExceptionVsError.ErrorAsEitherLeft                              thrpt   10  272316.153 ± 3188.632  ops/ms
[info] ExceptionVsError.ExceptionThrowingAndCatching                   thrpt   10       1.731 ±    0.006  ops/ms
[info] ExceptionVsError.ExceptionThrowingAndCatchingWithoutStackTrace  thrpt   10       1.612 ±    0.132  ops/ms
[info] ExceptionVsError.ExceptionThrowingWithCause                     thrpt   10       1.457 ±    0.156  ops/ms
[info] ExceptionVsError.FailureException                               thrpt   10    1215.788 ±   87.588  ops/ms
[info] ExceptionVsError.Retrowing                                      thrpt   10   35961.931 ±  119.702  ops/ms
 */
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class ExceptionVsError {

  val n = 1

  def theActionToBeDone: Unit = {
    for { i <- 0 to n } yield i
  }

  @Benchmark
  def ExceptionThrowingAndCatching: Try[Unit] = {
    Try {
      for { i <- 0 to n } yield i
      throw AnException("This is some common error but I will treat it as exceptional!! MOUAHAHAHAHAH!")
    }
  }

  @Benchmark
  def ExceptionThrowingAndCatchingWithoutStackTrace: Try[Unit] = {
    Try {
      for { i <- 0 to n } yield i
      throw ANoTraceException("I don't want your trace!")
    }
  }

  @Benchmark
  def ExceptionThrowingWithCause: Try[Unit] = {
    Try {
      for { i <- 0 to n } yield i
      throw AnExceptionWithCause("toto", ANoTraceException("I don't want your trace!"))
    }
  }

  @Benchmark
  def FailureException: Try[Unit] = {
    for { i <- 0 to n } yield i
    Failure(AnException("I Already know I failed!"))
  }

  @Benchmark
  def ErrorAsEitherLeft: Either[RuntimeError.type, Nothing] = {
    for { i <- 0 to n } yield i
    Left(RuntimeError)
  }

  @Benchmark
  def Retrowing: Try[Unit] = {
    Try {
      Try {
        for { i <- 0 to n } yield i
        throw AnException("I am exceptional!")
      } match {
        case Failure(e) => throw e
        case _          => ()
      }
    }
  }
}

case class AnExceptionWithCause(msg: String, e: Throwable) extends RuntimeException(msg, e)

case class AnException(msg: String) extends RuntimeException(msg)

abstract class NoStackTrace(msg: String) extends RuntimeException(msg) {
  override def fillInStackTrace() = this
}

case class ANoTraceException(msg: String) extends NoStackTrace(msg)

class BaseError(msg: String, exception: Option[Throwable] = None)
case class RuntimeError() extends BaseError("he felt")