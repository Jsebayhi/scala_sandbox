package sandbox.scalapl.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

//  sbt "project scala-pl" "jmh:run sandbox.scalapl.benchmarks.PlaygroundBench -wi 10 -i 5 -f1 -t1"
/*
wi: 10 i:5
*/

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class PlaygroundBench {

  @Benchmark
  def bench: Unit = {
  }

}