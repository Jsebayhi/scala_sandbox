package sandbox.scalapl.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

//  sbt "project scala-pl" "jmh:run sandbox.scalapl.benchmarks.StringConcatenationBench -wi 10 -i 5 -f1 -t1"
/*
wi: 20 i:20
[info] Benchmark                                            Mode    Cnt       Score       Error   Units
[info] StringConcatenationBench.stringAddition              thrpt   10      33612.738 ± 1814.278  ops/ms
[info] StringConcatenationBench.stringInterpolation         thrpt   10      12857.993 ±  223.712  ops/ms
[info] StringConcatenationBench.advancedStringInterpolation thrpt   10      12264.375 ±  498.066  ops/ms
*/

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class StringConcatenationBench {
  val toto = "toto"
  val titi = "titi"

  @Benchmark
  def stringAddition = toto + titi

  @Benchmark
  def stringInterpolation = s"${toto}${titi}"

  @Benchmark
  def advancedStringInterpolation = s"""$toto$titi"""

}