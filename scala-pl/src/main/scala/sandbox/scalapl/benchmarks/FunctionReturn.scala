package sandbox.scalapl.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

//  sbt "project scala-pl" "jmh:run sandbox.scalapl.benchmarks.FunctionReturn -wi 10 -i 5 -f1 -t1"
/*
for n = 1000
wi: 10 i:5
[info] Benchmark                                  Mode  Cnt   Score   Error   Units
[info] FunctionReturn.testDoEverythingFunction  thrpt    5  84.842 ± 0.879  ops/ms
[info] FunctionReturn.testDoOneSimpleThingWell  thrpt    5  83.834 ± 3.721  ops/ms

for n = 10
wi: 10 i:5
[info] Benchmark                                  Mode  Cnt     Score     Error   Units
[info] FunctionReturn.testDoEverythingFunction  thrpt    5  8175.015 ± 229.311  ops/ms
[info] FunctionReturn.testDoOneSimpleThingWell  thrpt    5  7631.369 ±  44.424  ops/ms

for n = 1
wi: 10 i:5
[info] Benchmark                                  Mode  Cnt      Score     Error   Units
[info] FunctionReturn.testDoEverythingFunction  thrpt    5  10457.982 ± 187.338  ops/ms
[info] FunctionReturn.testDoOneSimpleThingWell  thrpt    5  10122.213 ± 268.748  ops/ms
*/

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class FunctionReturn {

  val n = 1

  def theDoEverythingFunction: (String, String) = {
    for { i <- 0 to n } yield i
    for { i <- 0 to n } yield i

    ("toto", "tata")
  }

  def theDoOneThingWellFunctionForToto: String = {
    for { i <- 0 to n } yield i

    "toto"
  }

  def theDoOneThingWellFunctionForTata: String = {
    for { i <- 0 to n } yield i

    "tata"
  }

  @Benchmark
  def testDoEverythingFunction = {
    val (toto, tata) = theDoEverythingFunction
  }

  @Benchmark
  def testDoOneSimpleThingWell = {
    val toto = theDoOneThingWellFunctionForToto
    val tata = theDoOneThingWellFunctionForTata
  }
}
