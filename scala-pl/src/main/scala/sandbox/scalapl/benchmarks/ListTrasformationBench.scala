package sandbox.scalapl.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

//  sbt "project scala-pl" "jmh:run sandbox.scalapl.benchmarks.PlaygroundBench -wi 10 -i 5 -f1 -t1"
/*
wi: 10 i:5
*/

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class ListTrasformationBench {

  val inputList: List[Datum] = {
    for { i <- -1000 to 1000000 } yield Datum(i, i.toString)
  }.toList

  @Benchmark
  def transformThenToMap: Map[String, TransformedDatum] = {
    inputList.flatMap { datum =>
      TransformedDatum(datum).map(td => (td.iAsString, td))
    }.toMap
  }

  @Benchmark
  def transform: List[TransformedDatum] = {
    inputList.flatMap { datum =>
      TransformedDatum(datum)
    }
  }

  @Benchmark
  def toMap: Map[String, Datum] = {
    inputList.map { datum =>
      (datum.iAsString, datum)
    }.toMap
  }

}

case class TransformedDatum(i: Int, iAsString: String)
object TransformedDatum {
  def apply(d: Datum): Option[TransformedDatum] = {
    if (d.i >= 0) Some(new TransformedDatum(d.i, d.iAsString))
    else None
  }
}

case class Datum(i: Int, iAsString: String)