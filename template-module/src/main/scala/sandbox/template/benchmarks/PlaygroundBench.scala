package sandbox.template.benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._
import sandbox.template.Playground

//  sbt "project template-module" "jmh:run sandbox.template.benchmarks.PlaygroundBench -wi 10 -i 5 -f1 -t1"
/*
wi: 10 i:5
[info] Benchmark              Mode  Cnt  Score   Error   Units
[info] PlaygroundBench.main  thrpt    5  5.979 ± 2.639  ops/ms
 */
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.Throughput))
@State(Scope.Benchmark)
class PlaygroundBench {

  object PlaygroundSingleton extends Playground

  @Benchmark
  def main = PlaygroundSingleton.main(new Array(0))
}
